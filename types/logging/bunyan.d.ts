import bunyan, { LogLevel } from 'bunyan';
export declare const getBunyanLogLevel: () => LogLevel;
export declare const getBunyanLogger: (appId: string, level?: bunyan.LogLevel | undefined) => bunyan;
