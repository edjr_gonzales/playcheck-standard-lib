import { BaseEthAccount, EthAccount } from "./accounts";
export declare const LegacyDynamoDB: {
    BaseEthAccount: typeof BaseEthAccount;
    EthAccount: typeof EthAccount;
};
