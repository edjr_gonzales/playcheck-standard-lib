export interface ApprovalStatus {
    sideChain?: {
        [id: number]: boolean;
    };
    mainChain?: {
        [id: number]: boolean;
    };
    sideChainNetId?: string | number;
    sideChainId?: string | number;
    ethereumChainId?: string | number;
    ethereumNetId?: string | number;
}
export declare class BaseEthAccount {
    ethWallet: string;
    approvalStatus?: ApprovalStatus;
    mainChainParts?: number;
    sideChainParts?: number;
    partsCountedOn?: number;
    static loadByEthWallet(ethWallet: string): Promise<BaseEthAccount>;
}
export declare class EthAccount extends BaseEthAccount {
    signHash?: string;
    secretHash?: string;
    sessionToken?: string;
    sessionTime?: string;
    static loadByEthWallet(ethWallet: string): Promise<EthAccount>;
}
