import { DataMapper } from '@aws/dynamodb-data-mapper';
import DynamoDB from 'aws-sdk/clients/dynamodb';
export declare const DEFAULT_AWS_REGION = "ap-southeast-1";
export declare const DEFAULT_AWS_API = "2012-08-10";
export declare const DEFAULT_DB_NAMESPACE = "dev-";
export declare const AWS_API_VERSION: string;
export declare const AWS_ACCESS_KEY_ID: string;
export declare const AWS_SECRET_ACCESS_KEY: string;
/** Service connection use for AWS */
export declare const getService: () => DynamoDB;
/** You can use this for raw dynamoDB interactions */
export declare const getClient: () => DynamoDB.DocumentClient;
/** You can use this for mapper dynamoDB interactions */
export declare const getMapper: () => DataMapper;
export * from "./legacy";
