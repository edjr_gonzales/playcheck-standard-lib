const DevEnvList = ["development", "dev", "test"];
const ProdEnvList = ["production", "prod", "staging", "stage"];
const TestEnvList = ["test", "testing"];

export const isDevelopment = (env?:string) => {
  return DevEnvList.indexOf(env || process.env.NODE_ENV || "development") >= 0;
}

export const isProduction = (env?:string) => {
  return ProdEnvList.indexOf(env || process.env.NODE_ENV || "production") >= 0;
}

export const isTest = (env?:string) => {
  return TestEnvList.indexOf(env || process.env.NODE_ENV || "test") >= 0;
}