export interface ClassConstructor<T> {
  new (...args: any[]): T;
}

export interface EmptyConstructor<T> {
  new (): T;
}

export interface StringMap {
  [key: string]: string;
}

export interface NumberMap {
  [key: string]: number;
}