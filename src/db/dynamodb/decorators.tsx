import "reflect-metadata";

import { DocumentClient, Key } from "aws-sdk/clients/dynamodb";
import { delItemRecord, dynamoDBTable, getItemRecord, putItemRecord, queryItem, queryItems, scanItems, updateItemRecord } from ".";
import { ancestors, ClassConstructor, EmptyConstructor, getBunyanLogger, isNothing, isPrimitive, n2h, obj2Type, StringMap } from "../..";
import { isDeepStrictEqual } from "util";
import Logger from "bunyan";
import { PromiseResult } from "aws-sdk/lib/request";
import { AWSError } from "aws-sdk";
import crypto from 'crypto';

const logger = getBunyanLogger(`AWS#Decorator`);

interface ColumnValueMap {
  [column: string] :any;
}

interface DynamoDbDecoratorRuntimeBase {
  tableName: string;
  hashColumn?: {
    prop: string;
    column: string;
  };
  rangeColumn?: {
    prop: string;
    column: string;
  };
  localColumns: {
    [prop: string]: string;
  },
  allColumns: {
    [prop: string]: string;
  },
  inheritances: {
    [constructorName:string]: DynamoDbDecoratorRuntimeBase
  }
}

interface DynamoDbDecoratorRuntimeStruct {
  [constructorName:string] : DynamoDbDecoratorRuntimeBase
}

interface GenericMap<T> {
  [key: string]: T
}

export class MapDocumentColumn<T> {
  __content: GenericMap<T> = {};
  __changed: string[] = [];

  load(columnValue: GenericMap<T>, changed?:string []){
    this.__content = columnValue;
    this.__unstage();

    if(changed && changed.length > 0){
      this.__changed.push(...changed);
    }
  }

  __unstage(){
    // this unmarks all changes on
    this.__changed = []; // unstage changes
  }

  keys(){
    return Object.keys(this.__content);
  }

  value(){
    return this.__content;
  }

  // a little helper to obtain only relative updates
  getUpdates(){
    if(this.__changed.length === 0){
      return false;
    } else {
      const deleted: string[] = [];
      const changed:GenericMap<T> = {};

      for(const prop of this.__changed){
        if(!(prop in this.__content)){
          deleted.push(prop);
        } else {
          changed[prop] = this.__content[prop];
        }
      }

      return { deleted, changed };
    }
  }
}

const MapColumnHandler = <T extends unknown>() => ({
  get: function(obj:MapDocumentColumn<T>, prop:string) {
    // redirect return for existing functions and properties
    if(prop in obj){
      return (obj as any)[prop];
    }

    if(typeof prop !== 'string'){
      throw new Error(`Hash column accessor cannot by of type ${typeof prop}`);
    }

    // The default behavior to return the value
    return obj.__content[prop];
  },
  set: function(obj:MapDocumentColumn<T>, prop:string, value:any) {
    if(prop in obj){
      (obj as any)[prop] = value;

    } else {
      if(typeof prop !== 'string'){
        return false;
      }
  
      // The default behavior to store the value
      if(!isDeepStrictEqual(obj.__content[prop], value)){
        obj.__content[prop] = value;
        obj.__changed.push(prop);
      }
    }

    return true;
  },
  deleteProperty: function (obj:MapDocumentColumn<T>, prop:string) {
    if (!(prop in obj.__content)) { return false; }

    delete obj.__content[prop];
    obj.__changed.push(prop);

    return true;
  },
});

export class ListDocumentColumn<T> {
  __original: T[] = [];
  __content: T[] = [];

  load(columnValue: Array<T>){
    ////console.log("\t ListDocumentColumn loading value ", columnValue);
    if(this.__content.length)
      this.__content.splice(0, this.__content.length);
    
    columnValue.forEach((v, i) => this.__content[i] = v);
    this.__unstage();

    ////console.log("\t ListDocumentColumn loaded value ", this.__content);
  }

  __unstage(){
    // this unmarks all changes on
    this.__original = this.__content.map(i => i); // unstage changes
  }

  value(){
    return this.__content;
  }

  getUpdates(){
    ////////console.log(`get list changes`, this.__content, this.__original);

    // compare each items 1 by 1
    // yeah I know, this is inefficient
    // but we need to save a great deal of write
    const changed: {
      [key: number] : T
    } = {};

    const added: T[] = [];
    const removed: {
      [index: string]: T
    } = {};

    let trimmed = false;

    for(let i = 0; i < (this.__content.length > this.__original.length ? this.__content.length : this.__original.length); i++){
      const _old = this.__original[i];
      const _new = this.__content[i];

      if(!!_old && !!_new){
        if(!isDeepStrictEqual(_old, _new)){
          changed[i] = _new;
        }
      } else if(!_old && !!_new) {
        // no more matching item on index on old array
        added.push(_new);
      } else if(!!_old && !_new) {
        trimmed = this.__original.length > 0;
        removed[i] = _old;
      }
    }

    /**
     * NOTE:
     *  if trimmed, just rewrite the whole column
     *  if not trimmed, send the changes, then
     *    send the additions
     */
    return { changed, added, removed, trimmed, isNew: this.__original.length === 0 };
  }
}

const ArrayColumnHandler = <T extends unknown>() => ({
  get: function(obj:ListDocumentColumn<T>, prop:any) {
    // redirect return for existing functions and properties
    if(prop in obj){
      return (obj as any)[prop];
    }

    /* if(typeof prop !== 'number'){
      throw new Error(`Array column accessor cannot by of type ${typeof prop} - ${prop}`);
    } */

    // The default behavior to return the value
    return obj.__content[prop];
  },
  set: function(obj:ListDocumentColumn<T>, prop:any, value:any) {
    if(prop in obj){
      (obj as any)[prop] = value;
    } else {
      /* if(typeof prop !== 'number'){
        return false;
      } */
      
      // The default behavior to store the value
      if(!isDeepStrictEqual(obj.__content[prop], value)){
        obj.__content[prop] = value;
      }
    }

    return true;
  }
});

const DynamoDbDecoratorRuntimeDB:DynamoDbDecoratorRuntimeStruct = {}

export const DynamoDbTableHash = (columnName?:string) => {
  const decorator = (target:any, propertyKey:string) => {
    
    const key:string = target.constructor.name;
    const mapping = DynamoDbDecoratorRuntimeDB[key] ||= {
      tableName: key, // temp name mapping until overrriden by @DynamoDbTable
      localColumns: {},
      allColumns: {},
      inheritances: {},
    };

    if(mapping.hashColumn){
      throw new Error(`Set Hash Key ${propertyKey}: ${mapping.hashColumn.column} is already declared hash key for class ${key} table ${mapping.tableName}.`);
    }

    mapping.hashColumn = {
      column: columnName || propertyKey,
      prop: propertyKey
    }

    Object.defineProperty(target, propertyKey, {
      get: function(){
        //////console.log(`GET HASH ${propertyKey}`, this[`_${propertyKey}`]);
        return this[`_${propertyKey}`]
      },
      set: function(next){
        //////console.log(`SET HASH ${propertyKey}`, next);

        const _val = this[`_${propertyKey}`];
        const _new = this._isNew;
        const _empty = isNothing(_val);

        // changeable only if empty or hasn't been saved
        if( _empty || ( !_empty && _new ) ){
          this[`_${propertyKey}`] = next;
          // this && this.setChanged && this.setChanged(propertyKey);
        }
      },
      enumerable: true,
      configurable: true,
    });
  }

  return decorator;
}

export const DynamoDbTableRange = (columnName?:string) => {
  const decorator = (target:any, propertyKey:string) => {
    
    const key:string = target.constructor.name;
    const mapping = DynamoDbDecoratorRuntimeDB[key] ||= {
      tableName: key, // temp name mapping until overrriden by @DynamoDbTable
      localColumns: {},
      allColumns: {},
      inheritances: {},
    };

    if(mapping.rangeColumn){
      throw new Error(`Set Range Key ${propertyKey}: ${mapping.rangeColumn.column} is already declared range key for class ${key} table ${mapping.tableName}.`);
    }

    mapping.rangeColumn = {
      column: columnName || propertyKey,
      prop: propertyKey
    }

    Object.defineProperty(target, propertyKey, {
      get: function(){
        //////console.log(`GET RANGE ${propertyKey}`, this[`_${propertyKey}`]);
        return this[`_${propertyKey}`]
      },
      set: function(next){
        //////console.log(`SET RANGE ${propertyKey}`, next);

        const _val = this[`_${propertyKey}`];
        const _new = this._isNew;
        const _empty = isNothing(_val);

        // changeable only if empty or hasn't been saved
        if( _empty || ( !_empty && _new ) ){
          this[`_${propertyKey}`] = next;
          // this && this.setChanged && this.setChanged(propertyKey);
        }
      },
      enumerable: true,
      configurable: true,
    });
  }

  return decorator;
}

export interface DynamoDbTableAttrOpts {
  columnName? :string;
  castTo?: {
    type?: 'Array' | 'Map' | 'Hash' | 'CustomClass';
    memberType: ClassConstructor<any>;
  }
}

export const DynamoDbTableAttr = (optsOrColumnName?:DynamoDbTableAttrOpts | string) => {
  const decorator = (target:any, propertyKey:string) => {
    const reqColumnName = typeof optsOrColumnName === 'string' ? optsOrColumnName : ( optsOrColumnName?.columnName || propertyKey );

    logger.debug("Setting Property Decorator Attr", reqColumnName, " For ", target);

    const { castTo }:DynamoDbTableAttrOpts = ( optsOrColumnName === undefined || (typeof optsOrColumnName === 'string')) ? { columnName: reqColumnName } : optsOrColumnName;

    const key:string = target.constructor.name;
    const mapping = DynamoDbDecoratorRuntimeDB[key] ||= {
      tableName: key, // temp name mapping until overrriden by @DynamoDbTable
      localColumns: {},
      allColumns: {},
      inheritances: {},
    };

    if(mapping.localColumns[propertyKey]){
      throw new Error(`Column ${reqColumnName}: Duplicate column mapping for class table ${target}#${propertyKey}.`);
    }
    
    mapping.localColumns[propertyKey] = reqColumnName;
    
    Object.defineProperty(target, propertyKey, {
      get: function(){
        return this[`_${reqColumnName}`];
      },
      set: function(next){
        //////console.log(`SET ATTR ${propertyKey}`, next);

        let _next = next;

        // ready to cast if specified
        if(castTo && !isNothing(next)){
          const { type, memberType } = castTo;

          if(type === 'Array'){
            //////console.log(`SET ATTR Array ${propertyKey}`, next);
            const transformedSet: any[] = [];

            (next as Array<any>).forEach(($obj, i) => {
              if($obj instanceof memberType){
                transformedSet.push($obj);
              } else {
                transformedSet.push(obj2Type($obj, memberType));
              }
            });

            //////console.log(`SET ATTR Array ${propertyKey} transformedSet`, transformedSet);
            
            if( !this[`_${reqColumnName}`] ){
              //////console.log(`SET ATTR Array ${propertyKey} new list proxy instance`);

              // uninitialized as proxy, means a value load
              this[`_${reqColumnName}`] = new Proxy(
                new ListDocumentColumn<typeof memberType>(), 
                ArrayColumnHandler<typeof memberType>());

              this[`_${reqColumnName}`].load(transformedSet);

            } else {
              //////console.log(`SET ATTR Array ${propertyKey} update list proxy instance`);

              // already a proxy
              transformedSet.forEach((v, i) => {
                this[`_${reqColumnName}`][i] = v;
              });
            }
            
          } else if(type === 'Hash' || type === 'Map'){
            const _newHash:GenericMap<typeof memberType> = {};

            Object.keys(next).forEach((key, idx) => {
              if(next[key] instanceof memberType){
                _newHash[key] = next[key];
              } else {
                ////////console.log(`property ${propertyKey}.${key} cast `, next[key], memberType);
                _newHash[key] = obj2Type(next[key], memberType);
              }
            });

            /* //////console.log(`_${reqColumnName}`, this[`_${reqColumnName}`]);
            //////console.log(`_${reqColumnName} keys`, this[`_${reqColumnName}`].keys);
            //////console.log(`_${reqColumnName} keys value `, this[`_${reqColumnName}`].keys()); */

            if( !this[`_${reqColumnName}`] ){
              // not yet a proxy, indicates a load value
              this[`_${reqColumnName}`] = new Proxy(
                new MapDocumentColumn<typeof memberType>(),
                MapColumnHandler<typeof memberType>()
              );

              this[`_${reqColumnName}`].load(_newHash);

            } else {
              // already a proxy, updating values
              for(const nextProp in next){
                this[`_${reqColumnName}`][nextProp] = next[nextProp];
              }
            } 
            
          } else { // is CustomClass or undefined type
            if(!(next instanceof memberType)){
              _next = obj2Type(next, memberType);
            }

            if(!isDeepStrictEqual(_next, this[propertyKey])){
              this[`_${reqColumnName}`] = _next;
            }
          }

          return true;
        }

        if(!isDeepStrictEqual(_next, this[propertyKey])){
          this[`_${reqColumnName}`] = _next;
        }

        return true;
      },
      enumerable: true,
      configurable: true,
    });

    
  }

  return decorator;
}

export interface IDynamoDbTable<T> {
  new (...args: any[]): T;

  load: ( data?:DocumentClient.AttributeMap ) => Promise<T>;
  save: (simulation?:boolean) => Promise<PromiseResult<DocumentClient.PutItemOutput | DocumentClient.UpdateItemOutput, AWSError>>;
  destroy: () => Promise<PromiseResult<DocumentClient.PutItemOutput | DocumentClient.DeleteItemOutput, AWSError>>;
  table: () => string;
  
  toJSON: () => any;
  inspectDynamo: () => void;
}

export const DynamoDbTable = (dynamoTableName?:string) => {
  const decorator = <T extends { new(...args: any[]): {} }>(BaseClass: T) => {
    const tableName = dynamoTableName || BaseClass.name;
    const mappingName = BaseClass.name;

    logger.debug("Setting Class Decorator ", tableName, " For ", mappingName, BaseClass.prototype);

    const dynamoDbDecoratorMap = DynamoDbDecoratorRuntimeDB[mappingName] ||= {
      tableName: tableName,
      localColumns: {},
      allColumns: {},
      inheritances: {},
    };

    // force this
    dynamoDbDecoratorMap.tableName = tableName;

    const DynamoDbMappedClass = class extends BaseClass {
      // instance members 

      _isNew:boolean = true;
      _logger:Logger;

      //_changed: string[] = [];
      _memo:StringMap = {};

      _self:any = null;

      constructor(...args: any[]){
        super(...args);

        this._logger = getBunyanLogger(`AWS#${tableName}`, { env: process.env.NODE_ENV });
        this._self = this;
        //this._changed ||= [];
        this._memo = {};

        this._checkInheritance();
      }

      _checkInheritance(){
        //this._logger.debug(`Inheritance check! ${mappingName}`, dynamoDbDecoratorMap);

        const inheritances = ancestors(BaseClass);

        //this._logger.debug(`inheritances `, inheritances);

        // local class members parsed?
        if(Object.keys( dynamoDbDecoratorMap.allColumns ).length == 0){
          //this._logger.debug(`${mappingName} local columns`, dynamoDbDecoratorMap.localColumns);
          dynamoDbDecoratorMap.allColumns = { ...dynamoDbDecoratorMap.localColumns }
        }

        inheritances.forEach(theClass => {
          const constName = `${theClass.name}`;

          //this._logger.debug(`inheritance `, constName);

          if(constName !== mappingName && constName !== "Object" && constName !== "" && constName !== "undefined"){

            // check if not yet mapped
            //this._logger.debug(`\t${constName} is mapped`, !!dynamoDbDecoratorMap.inheritances[constName]);

            if(!dynamoDbDecoratorMap.inheritances[constName]){ 
              //this._logger.debug(`\t${constName} processing`);

              // inheritance not parsed
              const inheritance = DynamoDbDecoratorRuntimeDB[constName];

              if(inheritance){
               // this._logger.debug(`${mappingName} inheriting columns from ${constName}`, inheritance.localColumns);

                const { localColumns: inheritedLocalColumns, hashColumn, rangeColumn } = inheritance;

                // mark as mapped
                dynamoDbDecoratorMap.inheritances[constName] = inheritance; // not copy but reference

                // merge members
                dynamoDbDecoratorMap.allColumns = {
                  ...dynamoDbDecoratorMap.allColumns,
                  ...inheritedLocalColumns
                }

                // map hash column if not present
                if(!dynamoDbDecoratorMap.hashColumn){
                  dynamoDbDecoratorMap.hashColumn = hashColumn;
                }

                // map range column if not present
                if(!dynamoDbDecoratorMap.rangeColumn){
                  dynamoDbDecoratorMap.rangeColumn = rangeColumn;
                }

              }
            }
          }
        })
      }

      _allColumns(){
        const { allColumns, hashColumn, rangeColumn } = DynamoDbDecoratorRuntimeDB[mappingName];

        const cols = { ...allColumns };

        if(hashColumn){
          cols[hashColumn.prop] = hashColumn.column;
        }

        if(rangeColumn){
          cols[rangeColumn.prop] = rangeColumn.column;
        }

        return JSON.parse(JSON.stringify(cols)); // deep clone
      }

      _inspect(){
        const cols = this._allColumns();

        this._logger.info(`${mappingName} ${this.table()} inspect:`, cols);

        return cols;
      }

      public inspectDynamo(){
        this._logger.debug(`inspecting class ${mappingName} table ${this.table()}....`);
        this._inspect();
      }

      /**
       * memorizes the existing values 
       * then unmarks changes
       */
      _memoize(){
        this._memo = {};

        const allColumns = this._allColumns();

        for(let instanceProp in allColumns){
          const md5 = crypto.createHash('md5');
          const propValue = this._self[instanceProp];
          this._memo[instanceProp] = isPrimitive(propValue) ? `${propValue}` : md5.update(JSON.stringify(propValue)).digest('hex');

          if(this._self[instanceProp] instanceof MapDocumentColumn || this._self[instanceProp] instanceof ListDocumentColumn){
            this._self[instanceProp].__unstage();
          }
        }
      }

      _isChanged(mappedProp:string) :boolean {
        if(!this._memo[mappedProp] && this._self[mappedProp]) return true;

        const md5 = crypto.createHash('md5');

        const val = this._self[mappedProp];
        const sig = isPrimitive(val) ? `${val}` : md5.update(JSON.stringify(val)).digest('hex');

        return sig !== this._memo[mappedProp];
      }

      _clearData() {
        const allColumns = this._isNew ? this._allColumns() : dynamoDbDecoratorMap.allColumns;
        for(const propName in allColumns){
          this._self[propName] = undefined;
        }
      }

      _buildData(changedOnly:boolean = true): ColumnValueMap {
        const columnValueMap:ColumnValueMap = {};
        
        //this._logger.debug("_buildData dynamoDbDecoratorMap", dynamoDbDecoratorMap);

        const allColumns = this._isNew ? this._allColumns() : dynamoDbDecoratorMap.allColumns;
        for(const propName in allColumns){
          if(changedOnly && !this._isChanged(propName)){
            continue;
          }

          if(!this._isNew && this._self[propName] instanceof MapDocumentColumn){
            const updates = (this._self[propName] as MapDocumentColumn<any>).getUpdates();

            this._logger.debug(`Map Column ${propName} updates`, updates);

            if(updates){
              const { deleted, changed } = updates;

              columnValueMap[`${allColumns[propName]}`] = {};

              for(const changedKey in changed){
                columnValueMap[`${allColumns[propName]}`][changedKey] = changed[changedKey];
              }

              for(const deletedKey of deleted){
                columnValueMap[`${allColumns[propName]}`][deletedKey] = undefined;
              }
            }

          } else if(!this._isNew && this._self[propName] instanceof ListDocumentColumn){
            const updates = (this._self[propName] as ListDocumentColumn<any>).getUpdates();

            this._logger.debug(`List Column ${propName} updates`, updates);

            if(updates){
              const { added, removed, changed, trimmed, isNew } = updates;

              columnValueMap[`LIST ${allColumns[propName]}`] = {
                added,
                removed,
                changed
              };
            }

          } else {
            columnValueMap[allColumns[propName]] = this._self[propName];
          }
          
        }

        this._logger.debug("_buildData columnValueMap", columnValueMap);

        return columnValueMap;
      }

      _checkHash():boolean {
        const hash = dynamoDbDecoratorMap.hashColumn;

        if(!hash || !this._self[hash.prop]) {
          this._logger.debug("Hash key column and/or value is empty.", hash, hash && this._self[hash.prop]);
          throw new Error("Hash key column and/or value is empty.");
        }

        return true;
      }

      _checkRange():boolean {
        const range = dynamoDbDecoratorMap.rangeColumn;

        if(range && !this._self[range.prop]){
          this._logger.debug("Range key was defined but range key column and/or value is empty.", range, range && this._self[range.prop]);
          throw new Error("Range key was defined but range key column and/or value is empty.");
        }

        return true;
      }

      public async load( data?:DocumentClient.AttributeMap ){
        let result: DocumentClient.AttributeMap | undefined;

        const { hashColumn, rangeColumn, allColumns } = dynamoDbDecoratorMap;

        if(data){
          this._isNew = true;

          for(const propName in allColumns){
            this._self[propName] = data[allColumns[propName]];
          }

          if(hashColumn){
            this._self[hashColumn.prop] = data[hashColumn.column];
          }
          
          if(rangeColumn){
            this._self[rangeColumn.prop] = data[rangeColumn.column];
          }

          this._isNew = false;
          result = data;

        } else {
          // load record by hash key (range key)
          this._checkHash();
          this._checkRange();

          const key:Key = {};

          if(hashColumn){
            key[hashColumn?.column] = this._self[hashColumn?.prop];
          }
          
          if(rangeColumn){
            key[rangeColumn?.column] = this._self[rangeColumn?.prop];
          }

          const loadResult = await getItemRecord(this.table(), key, Object.values(this._allColumns()) );

          this._logger.debug('load result', loadResult);

          if(loadResult && loadResult.Item){
            result = loadResult.Item;

            // parse data
            for(const propName in allColumns){
              ////////console.log('load prop ', propName, allColumns[propName], loadResult.Item[allColumns[propName]]);
              this._self[propName] = loadResult.Item[allColumns[propName]];
            }
            
            this._isNew = false;

          } else {
            this._isNew = true;
            return false;
          }
        }
        
        // this._logger.debug('memorizing...');
        // unmark changes
        // this._changed = [];
        this._memoize();

        // this._logger.debug('memorizing...done');

        return this;
      }

      public async save(simulation?:boolean){
        this._logger.debug('Saving Record');

        this._checkHash();
        this._checkRange();

        const { hashColumn, rangeColumn } = dynamoDbDecoratorMap;

        // gather columns
        const columnAndValues = this._buildData(!this._isNew);

        this._logger.debug("columnAndValues", columnAndValues);
        this._logger.debug(`Saving Record - Is New ${this._isNew}`, hashColumn && this._self[hashColumn?.prop], rangeColumn && this._self[rangeColumn?.prop]);

        let updateResult;

        if(this._isNew){
          // save new record
          if(hashColumn){
            columnAndValues[hashColumn.column] = this._self[hashColumn.prop];
          }

          if(rangeColumn){
            columnAndValues[rangeColumn.column] = this._self[rangeColumn.prop];
          }

          updateResult = await putItemRecord(this.table(), columnAndValues, !!simulation).then((result) => {
            this._isNew = false; // if saved
            return result;
          });

          // logger.debug("Create Result", updateResult);

        } else {
          // update record
          const key:Key = {};

          if(hashColumn){
            key[hashColumn?.column] = this._self[hashColumn?.prop];
          }
          
          if(rangeColumn){
            key[rangeColumn?.column] = this._self[rangeColumn?.prop];
          }

          updateResult = await updateItemRecord(this.table(), columnAndValues, key, !!simulation);

          // logger.debug("Update Result", updateResult);
        }

        // unmark changes
        // this._changed = [];
        this._memoize();

        return updateResult;
      }

      public async destroy(){
        this._checkHash();
        this._checkRange();

        const { hashColumn, rangeColumn } = dynamoDbDecoratorMap;

        const key:Key = {};

        if(hashColumn){
          key[hashColumn?.column] = this._self[hashColumn?.prop];
        }
        
        if(rangeColumn){
          key[rangeColumn?.column] = this._self[rangeColumn?.prop];
        }

        return await delItemRecord(this.table(), key).then((result) => {
          this._isNew = true; // this record is now an unsaved entry
          return result;
        });
        
      }

      public table(){
        return dynamoDBTable(tableName);
      }

      public toJSON(){
        const jsonValue:any = {};

        for(const propName in this._allColumns()){
          jsonValue[propName] = this._self[propName];
        }
        
        return jsonValue;
      }
    }

    return DynamoDbMappedClass;
  }

  return decorator;
}

// returns an async iteratable object
export async function* queryMappedClass<T extends IDynamoDbTable<T>>(queryInput:Omit<DocumentClient.QueryInput, 'TableName'>, tableClass: EmptyConstructor<T>){
  // force re-checking the inheritance by instantiating
  const _gc = new tableClass; // dont remove this useless line, this is a cheat code

  let mappingMetadata:DynamoDbDecoratorRuntimeBase | null = null;

  for(const ancestor of ancestors(tableClass)){
    if(DynamoDbDecoratorRuntimeDB[ancestor.name] && !mappingMetadata){
      mappingMetadata = DynamoDbDecoratorRuntimeDB[ancestor.name];
      break;
    }
  }

  if(mappingMetadata){
    const projections: string[] = [
      ...(Object.values( mappingMetadata.allColumns ))
    ];

    const rangeKey = mappingMetadata.rangeColumn?.column;
    if(rangeKey){
      projections.unshift(rangeKey);
    }

    const hashKey = mappingMetadata.hashColumn?.column;
    if(hashKey){
      projections.unshift(hashKey);
    }
    
    const input:DocumentClient.QueryInput = { 
      ...queryInput, 
      TableName: dynamoDBTable(mappingMetadata.tableName)
    };

    const placeholders: string[] = [];
    projections.forEach((attr, index) => {
      const placeholder = `#ATTR${n2h(index + 1, 5)}`;
      placeholders.push(`${placeholder}`);

      input.ExpressionAttributeNames ||= {};
      input.ExpressionAttributeNames[placeholder] = attr;
    });

    input.ProjectionExpression = placeholders.join(', ');

    logger.debug(`queryMappedClass`, tableClass, input);

    const queryIterator = queryItems(input);
    for await (let item of queryIterator){
      yield await (new tableClass).load(item);
    }
  }
}

export async function queryMappedItem<T extends IDynamoDbTable<T>>(queryInput:Omit<DocumentClient.QueryInput, 'TableName'>, tableClass: EmptyConstructor<T>){
  // force re-checking the inheritance by instantiating

  let result:T | false = false;

  const instance = new tableClass;

  let mappingMetadata:DynamoDbDecoratorRuntimeBase | null = null;

  for(const ancestor of ancestors(tableClass)){
    if(DynamoDbDecoratorRuntimeDB[ancestor.name] && !mappingMetadata){
      mappingMetadata = DynamoDbDecoratorRuntimeDB[ancestor.name];
      break;
    }
  }

  if(mappingMetadata){
    const projections: string[] = [
      ...(Object.values( mappingMetadata.allColumns ))
    ];

    const rangeKey = mappingMetadata.rangeColumn?.column;
    if(rangeKey){
      projections.unshift(rangeKey);
    }

    const hashKey = mappingMetadata.hashColumn?.column;
    if(hashKey){
      projections.unshift(hashKey);
    }

    const input:DocumentClient.QueryInput = { 
      ...queryInput, 
      TableName: dynamoDBTable(mappingMetadata.tableName),
    };

    const placeholders: string[] = [];
    projections.forEach((attr, index) => {
      const placeholder = `#ATTR${n2h(index + 1, 5)}`;
      placeholders.push(`${placeholder}`);

      input.ExpressionAttributeNames ||= {};
      input.ExpressionAttributeNames[placeholder] = attr;
    });

    input.ProjectionExpression = placeholders.join(', ');

    const item = queryItem(input);

    if (item){
      result = await instance.load(item);
    }
  }

  return result;
}

// returns an async iteratable object
export async function* scanMappedClass<T extends IDynamoDbTable<T>>(scanInput:Omit<DocumentClient.ScanInput, 'TableName'>, tableClass: EmptyConstructor<T>){
  // force re-checking the inheritance by instantiating
  const _gc = new tableClass; // dont remove this useless line, this is a cheat code

  let mappingMetadata:DynamoDbDecoratorRuntimeBase | null = null;

  const classAncestors = ancestors(tableClass);
  ////////console.log('classAncestors', classAncestors);
  for(const ancestor of classAncestors){
    ////////console.log('ancestor', ancestor);

    if(DynamoDbDecoratorRuntimeDB[ancestor.name] && !mappingMetadata){
      mappingMetadata = DynamoDbDecoratorRuntimeDB[ancestor.name];
      ////////console.log('found mapping ', mappingMetadata);
      break;
    }
  }
 
  if(mappingMetadata){
    const projections: string[] = [
      ...(Object.values( mappingMetadata.allColumns ))
    ];

    const rangeKey = mappingMetadata.rangeColumn?.column;
    if(rangeKey){
      projections.unshift(rangeKey);
    }

    const hashKey = mappingMetadata.hashColumn?.column;
    if(hashKey){
      projections.unshift(hashKey);
    }

    const input:DocumentClient.ScanInput = { 
      ...scanInput, 
      TableName: dynamoDBTable(mappingMetadata.tableName),
    };

    const placeholders: string[] = [];
    projections.forEach((attr, index) => {
      const placeholder = `#ATTR${n2h(index + 1, 5)}`;
      placeholders.push(`${placeholder}`);

      input.ExpressionAttributeNames ||= {};
      input.ExpressionAttributeNames[placeholder] = attr;
    });

    input.ProjectionExpression = placeholders.join(', ');

    ////console.log(`input params`, input);

    const scanIterator = scanItems(input);
    for await (let item of scanIterator){
      ////////console.log(`yield item`, item);
      yield await (new tableClass).load(item);
    }
  }
}