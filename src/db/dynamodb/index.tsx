import { Credentials } from 'aws-sdk';
import DynamoDB, { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { getBunyanLogger } from '../..';

require('dotenv').config();

export const DEFAULT_AWS_REGION = 'ap-southeast-1';
export const DEFAULT_AWS_API = '2012-08-10';
export const DEFAULT_DB_NAMESPACE = 'dev-';

export const AWS_API_VERSION = process.env.AWS_API_VERSION || DEFAULT_AWS_API;
export const AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID || "";
export const AWS_SECRET_ACCESS_KEY = process.env.AWS_SECRET_ACCESS_KEY || "";

const logger = getBunyanLogger('utils-aws-dynamodb', { env: process.env.NODE_ENV });

const dynamo:{
  service?:DynamoDB,
  client?:DocumentClient,
} = {}

/** Service connection use for AWS */
export const getService = () => {
  if(!dynamo.service){
    logger.info('Starting dynamodb...');
    logger.debug("\tAWS_REGION", process.env.AWS_REGION || DEFAULT_AWS_REGION);
    logger.debug("\tAWS_API_VERSION", AWS_API_VERSION);
    logger.debug("\tAWS_ACCESS_KEY_ID", AWS_ACCESS_KEY_ID);
    logger.debug("\tAWS_SECRET_ACCESS_KEY", AWS_SECRET_ACCESS_KEY);
    
    dynamo.service = new DynamoDB({
      endpoint: !!process.env.DYNAMO_ENDPOINT ? process.env.DYNAMO_ENDPOINT : undefined,
      region: process.env.AWS_REGION || DEFAULT_AWS_REGION,
      credentials: new Credentials(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY),
      apiVersion: AWS_API_VERSION
    });
  }
  
  return dynamo.service;
}

/** You can use this for raw dynamoDB interactions */
export const getDynamoDbDocClient = () => {
  if(!dynamo.client){
    logger.info('Starting dynamodb client...');

    dynamo.client = new DocumentClient({
      service: getService()
    });
  }
  
  return dynamo.client;
}

export const getDynamoTablePrefix = () => process.env.DYNAMO_TABLE_NAMESPACE || DEFAULT_DB_NAMESPACE;

export * from "./tests/testClass";
export * from "./legacy";
export * from "./helpers";
export * from "./decorators";