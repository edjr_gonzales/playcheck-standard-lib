import { DynamoDbTable, DynamoDbTableAttr, DynamoDbTableHash, IDynamoDbTable } from "../decorators";

@DynamoDbTable('Accounts')
export class WalletAccount {
  @DynamoDbTableHash()
  ethWallet: string = "";

  @DynamoDbTableAttr()
  signHash?: string;

  @DynamoDbTableAttr()
  secretHash?: string;

  @DynamoDbTableAttr()
  loginHash?: string;

  @DynamoDbTableAttr()
  sessionToken?: string;

  @DynamoDbTableAttr()
  sessionTime?: string;
}


@DynamoDbTable('Accounts')
export class AccountInfo extends WalletAccount {
  @DynamoDbTableAttr()
  displayName?: string;

  @DynamoDbTableAttr({ castTo: { type: 'Hash', memberType: Object } })
  userPref?: { [key: string]: Object; };
}

export type EthWalletAccountType = WalletAccount | AccountInfo;

export interface WalletAccount extends IDynamoDbTable<any> {}
export interface AccountInfo extends IDynamoDbTable<any> {}

export const loadWalletAccount = async (instance:EthWalletAccountType, ethWallet: string) => {
  const walletAccount = Object.assign(instance, { ethWallet });
  await walletAccount.load();
  return walletAccount;
}
