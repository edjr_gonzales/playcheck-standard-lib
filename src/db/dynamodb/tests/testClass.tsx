import { DynamoDbTable, IDynamoDbTable, DynamoDbTableAttr, DynamoDbTableHash, DynamoDbTableRange } from "../decorators";

export interface TestClass extends IDynamoDbTable<TestClass> {}
@DynamoDbTable('Test')
export class TestClass {
  @DynamoDbTableHash()
  testKey: string = '';

  @DynamoDbTableRange()
  testSort: string = '';

  @DynamoDbTableAttr({ castTo: { type: 'Hash', memberType: String } })
  stringMapAttr:{
    [key:string]:string;
  } = {};

  @DynamoDbTableAttr({ castTo: { type: 'Hash', memberType: Object } })
  objMapAttr:{
    [key:string]:Object;
  } = {};

  @DynamoDbTableAttr({ castTo: { type: 'Array', memberType: String } })
  stringListAttr:string[] = [];

  @DynamoDbTableAttr({ castTo: { type: 'Array', memberType: Object } })
  objListAttr:Object[] = [];
}