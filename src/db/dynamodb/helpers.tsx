import { AWSError } from "aws-sdk";
import { DeleteItemInput, DocumentClient, GetItemInput, Key, QueryInput, ScanInput, UpdateItemInput } from "aws-sdk/clients/dynamodb";
import { PromiseResult } from "aws-sdk/lib/request";
import { getDynamoDbDocClient, getDynamoTablePrefix, ListDocumentColumn, MapDocumentColumn } from ".";
import { n2h, getBunyanLogger, isNothing, isPrimitive } from "../..";

const client = getDynamoDbDocClient();
const logger = getBunyanLogger('DynamoDbHelpers', { env: process.env.NODE_ENV });

export const dynamoDBTable = (tableName:string) => {
  const prefix = getDynamoTablePrefix();

  if(!tableName.startsWith(prefix)){
    return `${prefix}${tableName}`;
  } else {
    return tableName;
  }
}

interface ListAttributeUpdates {
  [itemAttrName: string]: Array<Object | string | boolean | number>;
}
export const addToListAttr = async (tableName:string, objectData:ListAttributeUpdates, key:Key, simulation:boolean = false) => {
  const params:UpdateItemInput = {
    TableName: dynamoDBTable(tableName),
    Key: key,
    UpdateExpression: '',
    ExpressionAttributeNames: {},
    ExpressionAttributeValues: {},
    ReturnValues: "UPDATED_NEW"
  }

  const toAdd: string[] = [];

  Object.keys(objectData).forEach((prop, idx) => {
    const placeHolder = n2h(idx, 5);

    params.ExpressionAttributeValues ||= {}

    toAdd.push(`#${placeHolder} = list_append(#${placeHolder}, :${placeHolder})`);
    params.ExpressionAttributeValues[`:${placeHolder}`] = objectData[prop] as any;

    /* for(const entryValue of objectData[prop]){

      switch(typeof entryValue){
        case 'string':
          params.ExpressionAttributeValues[`:${placeHolder}`] = { SS: objectData[prop].map(s => s.toString()) };
          break;
        case 'number':
          params.ExpressionAttributeValues[`:${placeHolder}`] = { NS: objectData[prop].map(n => n.toString()) };
          break;
        default:
          params.ExpressionAttributeValues[`:${placeHolder}`] = objectData[prop] as any;
      }

      break;
    } */

    params.ExpressionAttributeNames ||= {}
    params.ExpressionAttributeNames[`#${placeHolder}`] = prop;
  });

  if(toAdd.length > 0){
    params.UpdateExpression = `${toAdd.join(', ')}`;
    logger.debug('Update List Append params', params);

    if(!simulation){
      return await client.update(params).promise();
    } else {
      return params;
    }
  }

  return false;
}

/**
 * 
 * @param tableName perform safe delete on list column
 * @param objectData 
 * @param key 
 * @returns 
 */
export const delFromListAttr = async (tableName:string, objectData:ListAttributeUpdates, key:Key, simulation:boolean = false) => {
  const simulationResult = [];

  const result:{
    [index: string]: boolean
  } = {};

  const params:UpdateItemInput = {
    TableName: dynamoDBTable(tableName),
    Key: key,
    UpdateExpression: '',
    ExpressionAttributeNames: {},
    ExpressionAttributeValues: {},
    ReturnValues: 'NONE'
  }

  let idx = 0;
  for(const prop in objectData){
    const placeHolder = n2h(idx, 5);

    params.ExpressionAttributeNames ||= {}
    params.ExpressionAttributeNames[`#${placeHolder}`] = prop;

    for(const indexToRemove in objectData[prop]){
      const indexPlaceholder = `${placeHolder}${n2h(Number(indexToRemove), 5)}`;

      params.UpdateExpression = `REMOVE #${placeHolder}[${indexToRemove}]`;
      params.ConditionExpression = `#${placeHolder}[${indexToRemove}] = :${indexPlaceholder}`

      params.ExpressionAttributeValues ||= {}
      params.ExpressionAttributeValues[`:${indexPlaceholder}`] = objectData[prop][indexToRemove] as any;  

      logger.debug('Update List Remove params', params);

      if(!simulation){
        await client.update(params).promise()
          .then(() => {
            result[indexToRemove] = true
          })
          .catch(() => {
            result[indexToRemove] = false
          });
      } else {
        simulationResult.push( JSON.parse(JSON.stringify(params)) );
      }
      
    }

    idx++;
  }

  if(!simulation) return result;

  return simulationResult;
}

interface MapAttributeUpdates {
  [itemAttrName: string]: {
    [mapAttrKey: string]: Array<any> | Object | string | boolean | number;
  }
}
export const updateMapAttr = async (tableName:string, objectData:MapAttributeUpdates, key:Key, simulation:boolean = false) => {
  const params:UpdateItemInput = {
    TableName: dynamoDBTable(tableName),
    Key: key,
    UpdateExpression: '',
    ExpressionAttributeNames: {},
    ExpressionAttributeValues: {},
    ReturnValues: "UPDATED_NEW"
  }

  const keyAttrs = Object.keys(key);

  const toRemove: string[] = [];
  const toSet: string[] = [];

  Object.keys(objectData).forEach((prop, idx) => {
    const placeHolder = n2h(idx, 5);

    if(!keyAttrs.includes(prop)){ // not key/hash column
      let mapTarget = prop;

      if( isNothing(objectData[mapTarget]) || Object.keys(objectData[mapTarget]) ){
        // remove the itemAttrName (column)
        toRemove.push(`#${placeHolder}`);

      } else {
        // traverse map column attributes

        // make sure that map column exists
        toSet.push(`#${placeHolder} = if_not_exists(#${placeHolder}, {})`);

        const columnAttributes = objectData[prop];
        Object.keys(columnAttributes).forEach((mapAttrKey, mapAttrIdx) => {
          const subPlaceHolder = n2h( ((idx + 1) * 1000) + mapAttrIdx, 5 );

          if( isNothing(objectData[mapTarget][mapAttrKey]) ){
            toRemove.push(`#${subPlaceHolder}`);

          } else {
            params.ExpressionAttributeValues ||= {}

            switch(typeof columnAttributes[mapAttrKey]){
              case 'string':
                params.ExpressionAttributeValues[`:${subPlaceHolder}`] = { S: `${columnAttributes[mapAttrKey]}` };
                break;
              case 'number':
                params.ExpressionAttributeValues[`:${subPlaceHolder}`] = { N: `${columnAttributes[mapAttrKey]}` };
                break;
              case 'boolean':
                params.ExpressionAttributeValues[`:${subPlaceHolder}`] = { BOOL: !!columnAttributes[mapAttrKey] };
                break;
              default:
                if(Array.isArray(columnAttributes[mapAttrKey])){
                  params.ExpressionAttributeValues[`:${subPlaceHolder}`] = { L: columnAttributes[mapAttrKey] as Array<any> };
                } else {
                  // Object
                  params.ExpressionAttributeValues[`:${subPlaceHolder}`] = { M: columnAttributes[mapAttrKey] as any };
                }
            }
          }

          params.ExpressionAttributeNames ||= {}
          params.ExpressionAttributeNames[`#${subPlaceHolder}`] = `${mapTarget}.${mapAttrKey}`;
        });
      }

      params.ExpressionAttributeNames ||= {}
      params.ExpressionAttributeNames[`#${placeHolder}`] = mapTarget;
    }
  });

  if(toSet.length > 0){
    params.UpdateExpression = `SET ${toSet.join(', ')}`;
  }

  if(toRemove.length > 0){
    params.UpdateExpression = `${params.UpdateExpression}${params.UpdateExpression !== '' ? ' ' : ''}REMOVE ${toRemove.join(', ')}`;
  }

  logger.debug('Update Map params', params);

  if(!simulation){
    return await client.update(params).promise();
  } else {
    return params;
  }
  
}

export const putItemRecord = async (tableName:string, objectData:any, simulation:boolean = false) => {
  const params = {
    TableName: dynamoDBTable(tableName),
    Item: objectData
  }

  const transformed: any = {};

  for(const prop in objectData){
    if(objectData[prop] instanceof ListDocumentColumn ||
      objectData[prop] instanceof MapDocumentColumn){
      transformed[prop] = objectData[prop].value();
    }
  }

  params.Item = { ...params.Item, ...transformed };

  logger.debug('Put params', params);

  if(!simulation){
    return await client.put(params).promise();
  } else {
    return params;
  }
  
}

export const updateItemRecord = async (tableName:string, objectData:any, key:Key, simulation:boolean = false) => {
  const alternateResult:any[] = [];

  const params:UpdateItemInput = {
    TableName: dynamoDBTable(tableName),
    Key: key,
    UpdateExpression: '',
    ExpressionAttributeNames: {},
    ExpressionAttributeValues: {},
    ReturnValues: "UPDATED_NEW"
  }

  const keyAttrs = Object.keys(key);

  const toRemove: string[] = [];
  const toSet: string[] = [];

  let idx = 0;
  for(const prop in objectData){
    const placeHolder = n2h(idx + 1, 5);

    if(!keyAttrs.includes(prop)){ // not partition/sort column
      let setTarget = prop;

      if(isNothing(objectData[prop])){
        toRemove.push(`#${placeHolder}`);

      } else {
        if(prop.startsWith('LIST ')){
          setTarget = prop.replace('LIST ', '');

          const { removed, changed, added } = objectData[prop];

          // send it on its own to avoid transactional memory

          // send deletions first, this will send its own commands
          // `removed` is expected to be a mapping of index to values
          const delResult = Object.keys(removed).length > 0 && await delFromListAttr(params.TableName, {
            [setTarget]: removed 
          }, key, simulation);
          alternateResult.push(delResult);
          logger.debug(`separate deletion result`, delResult);

          // send inserts
          const addResult = (added.length > 0) && await addToListAttr(params.TableName, {
            [setTarget]: added 
          }, key, simulation);
          alternateResult.push(addResult);
          logger.debug(`separate addition result`, addResult);

          // send updates
          // `changed` is expected to be a mapping of index to values
          for(const keyIndex in changed){
            const valuePlaceholder = `${placeHolder}${n2h(Number(keyIndex), 5)}`;

            toSet.push(`#${placeHolder}[${keyIndex}] = :${valuePlaceholder}`);

            params.ExpressionAttributeValues ||= {}
            params.ExpressionAttributeValues[`:${valuePlaceholder}`] = changed[keyIndex];
          }

        } else if(typeof objectData[prop] === 'object' && objectData[prop] instanceof Object && !Array.isArray(objectData[prop])){
          Object.keys(objectData[prop]).forEach((mapKey, mapIdx) => {
            const mapKeyHolder = `${placeHolder}${n2h(mapIdx, 5)}`;

            toSet.push(`#${placeHolder}.#${mapKeyHolder} = :${mapKeyHolder}`);

            params.ExpressionAttributeNames ||= {}
            params.ExpressionAttributeNames[`#${mapKeyHolder}`] = mapKey;

            params.ExpressionAttributeValues ||= {}
            params.ExpressionAttributeValues[`:${mapKeyHolder}`] = objectData[prop][mapKey];
          });

        } else {
          toSet.push(`#${placeHolder} = :${placeHolder}`);
  
          params.ExpressionAttributeValues ||= {}
          params.ExpressionAttributeValues[`:${placeHolder}`] = objectData[prop];
        }
      }

      params.ExpressionAttributeNames ||= {}
      params.ExpressionAttributeNames[`#${placeHolder}`] = setTarget;
    }

    idx++;
  }

  if(toSet.length > 0){
    params.UpdateExpression = `SET ${toSet.join(', ')}`;
  }

  if(toRemove.length > 0){
    params.UpdateExpression = `${params.UpdateExpression}${params.UpdateExpression !== '' ? ' ' : ''}REMOVE ${toRemove.join(', ')}`;
  }

  logger.debug('Update params', params);

  if(!simulation){
    return await client.update(params).promise();
  } else {
    alternateResult.push(params);
    return alternateResult;
  }
  
}

export const delItemRecord = async (tableName:string, key:Key, simulation:boolean = false) => {
  var params:DeleteItemInput = {
    TableName: dynamoDBTable(tableName),
    Key: key
  }

  logger.debug('Delete params', params);

  if(!simulation)
    return await client.delete(params).promise();

  return params;
}

export const getItemRecord = (tableName:string, key:Key, projectionExpr?:string[]) =>{
  var params:GetItemInput = {
    TableName: dynamoDBTable(tableName),
    Key: key
  }

  if(projectionExpr){
    const placeholders: string[] = [];
    projectionExpr.forEach((attr, index) => {
      const placeholder = `#ATTR${n2h(index + 1, 5)}`;
      placeholders.push(`${placeholder}`);

      params.ExpressionAttributeNames ||= {};
      params.ExpressionAttributeNames[placeholder] = attr;
    });

    params.ProjectionExpression = placeholders.join(', ');
  }

  logger.debug('Get params', params);

  return client.get(params).promise();
}

// returns an async iteratable object
export async function* queryItems(queryInput:QueryInput){
  let currentItem = 0;
  let currentSet = await client.query(queryInput).promise();
  let resultCount = currentSet.Count || 0;

  while(true){
    // we reached the end of items but we still have more based on presence of LastEvaluatedKey
    if(currentItem >= resultCount && !!currentSet.LastEvaluatedKey){
      currentSet = await client.query({...queryInput, ExclusiveStartKey: currentSet.LastEvaluatedKey}).promise();
      currentItem = 0;
      resultCount = currentSet.Count || 0;

    } else if(currentItem >= resultCount){
      break;

    } else {
      yield currentSet.Items && currentSet.Items[currentItem];
      currentItem++;
    }
  }
}

// returns an item object or false
export async function queryItem(queryInput:QueryInput){
  let currentSet = await client.query(queryInput).promise();
  let resultCount = currentSet.Count || 0;

  if(!currentSet.$response.error && resultCount > 0){
    return currentSet.Items && currentSet.Items[0];
  } else {
    return false;
  }
}

// returns an async iteratable object
export async function* scanItems(scanInput:ScanInput){
  //console.log(`Scan params`, scanInput);

  let currentItem = 0;
  let currentSet = await client.scan(scanInput).promise();
  let resultCount = currentSet.Count || 0;

  while(true){
    // we reached the end of items but we still have more based on presence of LastEvaluatedKey
    if(currentItem >= resultCount && !!currentSet.LastEvaluatedKey){
      currentSet = await client.scan({...scanInput, ExclusiveStartKey: currentSet.LastEvaluatedKey}).promise();
      currentItem = 0;
      resultCount = currentSet.Count || 0;

    } else if(currentItem >= resultCount){
      break;

    } else {
      yield currentSet.Items && currentSet.Items[currentItem];
      currentItem++;
    }
  }
}