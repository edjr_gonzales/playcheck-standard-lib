import bunyan, { INFO, DEBUG, TRACE, LogLevel } from 'bunyan';

export const getBunyanLogLevel = (env:string):LogLevel => {
  switch(env){
    case "development":
    case "dev":
      return DEBUG;
      
    case "test":
      return TRACE;

    default:
      return INFO;
  }
}

interface LoggerOptions { level?:LogLevel, env?:string }
export const getBunyanLogger = (appId:string, options?:LoggerOptions) => {
  const { level, env } = options || {};
  return bunyan.createLogger({name: appId, level: (!level ? getBunyanLogLevel(env || 'production') : level)  });
}
