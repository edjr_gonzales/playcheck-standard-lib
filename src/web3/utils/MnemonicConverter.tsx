import Web3 from "web3";
import Bunyan from "bunyan";
import { hdkey } from 'ethereumjs-wallet';

const bip39 = require("bip39");
const logger = Bunyan.createLogger({
  name: `MnemonicConverter`,
  level: process.env.NODE_ENV === "development" ? Bunyan.DEBUG : Bunyan.INFO,
});

export default async(mnemonic:string, addressIndex:number = 0, numAddresses:number = 1, derivationpath = "m/44'/60'/0'/0/") => {
  //logger.debug('Mnemonic: ', mnemonic, addressIndex, numAddresses, derivationpath);

  const addresses = [];
  const seed = await bip39.mnemonicToSeed(mnemonic);
  const hdwallet = await hdkey.fromMasterSeed(seed);

  //logger.debug(`seed`, seed);
  //logger.debug(`hdwallet`, hdwallet);

  for (let i = addressIndex; i < addressIndex + numAddresses; i++){
    const wallet = hdwallet.derivePath(derivationpath + i).getWallet();
    const addr = '0x' + wallet.getAddress().toString('hex');

    //logger.debug(`Addr -- `, addr);
    
    addresses.push(Web3.utils.toChecksumAddress(addr));
  }

  return addresses;
}