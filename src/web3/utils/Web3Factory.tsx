import Web3 from "web3";
import crypto from "crypto";

import HDWalletProvider from '@truffle/hdwallet-provider';
import ChainNetConfig from "../types/ChainNetConfig";
import { getBunyanLogger } from "../..";
import { HD_WALLET_PROVIDER, HD_WALLET_PROVIDER_WS, HTTP_PROVIDER, IPC_PROVIDER, WS_PROVIDER } from "..";

/* const WebsocketProvider = require('web3-providers-ws');
const HttpProvider = require('web3-providers-http');
const IpcProvider = require('web3-providers-ipc');

export type Web3Provider = Web3.providers.HttpProvider | Web3.providers.WebsocketProvider | Web3.providers.IpcProvider | HDWalletProvider; */

const logger = getBunyanLogger('web3-factory', { env: process.env.NODE_ENV } );

const web3CacheProviderIndex:{
  [web3Key:string]: number;
} = {};

const web3Cache:{
  [web3Key:string]: Web3;
} = {};

const baseWsOptions = {
  timeout: 30000, // ms

  // Useful for credentialed urls, e.g: ws://username:password@localhost:8546
  /* headers: {
    authorization: 'Basic username:password'
  }, */

  clientConfig: {
    // Useful if requests are large
    maxReceivedFrameSize: 100000000,   // bytes - default: 1MiB
    maxReceivedMessageSize: 100000000, // bytes - default: 8MiB

    // Useful to keep a connection alive
    keepalive: true,
    keepaliveInterval: 60000 // ms
  },

  // Enable auto reconnection
  reconnect: {
      auto: true,
      delay: 5000, // ms
      maxAttempts: 5,
      onTimeout: false
  }
};

export const web3ProviderFactory = (config:ChainNetConfig, providerType:("http" | "ws" | "ipc") = HTTP_PROVIDER) => {
  //logger.debug(config, providerType);

  const web3CacheKey = `${providerType}/${config.networkId}`;

  const wsUrls = config.wsUrl || [];
  const ipcUrl = config.ipcUrl || "";

  if(providerType === WS_PROVIDER && wsUrls.length == 0){
    throw(`Cannot use websocket providers when no WS Provider URL given.`);
  }

  if(providerType === IPC_PROVIDER && ipcUrl === ""){
    throw(`Cannot use IPC provider when no IPC Provider given.`);
  }

  let providerIndex = !web3CacheProviderIndex[web3CacheKey] ? 0 : web3CacheProviderIndex[web3CacheKey];
  let providerUrl = "";

  if(providerType !== IPC_PROVIDER){
    if((providerIndex + 1) >= (providerType === HTTP_PROVIDER ? config.httpUrl : wsUrls).length){
      providerIndex = 0;
    } else {
      //save the pointer and increment for next call
      web3CacheProviderIndex[web3CacheKey] = providerIndex + 1;
    }
    
    providerUrl = (providerType === HTTP_PROVIDER ? config.httpUrl : wsUrls)[providerIndex];
  } else {
    providerUrl = ipcUrl;
  }
  
  //logger.debug(`web3CacheProviderIndex[${web3CacheKey}]`, web3CacheProviderIndex[web3CacheKey]);
  logger.debug(`providerUrl = `, providerUrl);
  
  if(providerType === IPC_PROVIDER){
    const net = require('net');
    return new Web3.providers.IpcProvider(providerUrl, net);

  } else if(providerType === WS_PROVIDER){
    return new Web3.providers.WebsocketProvider(providerUrl, config.wsOptions || baseWsOptions);
    
  } else {
    return new Web3.providers.HttpProvider(providerUrl, config.httpOptions);
  }
}

export const httpProviderFactory = (config:ChainNetConfig) => {
  return web3ProviderFactory(config);
}

export const wsProviderFactory = (config:ChainNetConfig) => {
  return web3ProviderFactory(config, WS_PROVIDER);
}

export const ipcProviderFactory = (config:ChainNetConfig) => {
  return web3ProviderFactory(config, IPC_PROVIDER);
}

export const hdWalletProviderFactory = (config:ChainNetConfig, providerType:("http" | "ws") = "http") => {
  const web3CacheKey = `HDWallet/${providerType}/${config.networkId}`;
  const wsUrls = config.wsUrl || [];

  if(providerType === WS_PROVIDER && wsUrls.length == 0){
    throw(`Cannot use websocket providers when no WS Provider URL given.`);
  }

  let providerIndex = !web3CacheProviderIndex[web3CacheKey] ? 0 : web3CacheProviderIndex[web3CacheKey];
  
  if((providerIndex + 1) >= (providerType === HTTP_PROVIDER ? config.httpUrl : wsUrls).length){
    web3CacheProviderIndex[web3CacheKey] = 0;
  } else {
    web3CacheProviderIndex[web3CacheKey] = providerIndex + 1;
  }
  
  const providerUrl = (providerType === HTTP_PROVIDER ? config.httpUrl : wsUrls)[providerIndex];
  
  const providerOrUrl = providerType === HTTP_PROVIDER ? providerUrl : new Web3.providers.WebsocketProvider(providerUrl, config.wsOptions || baseWsOptions);
    
  return new HDWalletProvider({
    ...config.walletOptions,
    providerOrUrl
  });
}

export const hdWalletHttpProviderFactory = (config:ChainNetConfig) => {
  return hdWalletProviderFactory(config, HTTP_PROVIDER);
}

export const hdWalletWsProviderFactory = (config:ChainNetConfig) => {
  return hdWalletProviderFactory(config, WS_PROVIDER);
}

export const PROVIDER_FACTORIES = {
  [HTTP_PROVIDER]: httpProviderFactory,
  [WS_PROVIDER]: wsProviderFactory,
  [IPC_PROVIDER]: ipcProviderFactory,
  [HD_WALLET_PROVIDER]: hdWalletHttpProviderFactory,
  [HD_WALLET_PROVIDER_WS]: hdWalletWsProviderFactory
};

/**
 * creates web3 instance with provided provider or use connection balancing using multiple 
 * endpoint URLS
 */
export default (networkConfig:ChainNetConfig, providerFactory?:(config:ChainNetConfig) => any):Web3 => {
  const walletHash = crypto.createHash('md5').update(JSON.stringify(networkConfig.walletOptions)).digest('hex');

  const web3CacheKey = `${[HD_WALLET_PROVIDER, HD_WALLET_PROVIDER_WS].includes(networkConfig.useProvider) ? 'HDWallet/' : ''}${networkConfig.useProvider}/${networkConfig.networkId}`;
  const index = web3CacheProviderIndex[web3CacheKey] || 0;

  const keyBase = networkConfig.useProvider === HD_WALLET_PROVIDER ? networkConfig.httpUrl[index] : (
    networkConfig.useProvider === HD_WALLET_PROVIDER_WS && networkConfig.wsUrl ? networkConfig.wsUrl[index] : (
      networkConfig.useProvider === IPC_PROVIDER && networkConfig.ipcUrl ? networkConfig.ipcUrl : (
        networkConfig.useProvider === HTTP_PROVIDER ? networkConfig.httpUrl[index] : networkConfig.wsUrl && networkConfig.wsUrl[index]
      )
    )
  );

  logger.debug(`keyBase`, keyBase);

  const web3Key:string = `${networkConfig.useProvider}###${networkConfig.networkId}###${walletHash}###${keyBase}`;

  logger.debug(`web3CacheKey`, web3Key);

  if(!web3Cache[web3Key]){
    logger.debug(`creating web3 instance for key`, web3Key);

    if(providerFactory){
      web3Cache[web3Key] = new Web3(providerFactory(networkConfig));
    } else {
      web3Cache[web3Key] = new Web3(
        networkConfig.useProvider === HD_WALLET_PROVIDER ? hdWalletHttpProviderFactory(networkConfig) : (
          networkConfig.useProvider === HD_WALLET_PROVIDER_WS ? hdWalletWsProviderFactory(networkConfig) : (
            networkConfig.useProvider === IPC_PROVIDER ? ipcProviderFactory(networkConfig) : (
              networkConfig.useProvider === HTTP_PROVIDER ? httpProviderFactory(networkConfig) : wsProviderFactory(networkConfig)
            )
          )
        )
      );
    }
  }

  return web3Cache[web3Key];
}