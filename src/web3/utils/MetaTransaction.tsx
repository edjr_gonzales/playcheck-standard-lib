import Web3 from 'web3';

import { ChainNetConfig, createTypedSignature, EthereumAddress, getHDWalletProvider } from "..";

export const domainType = [
  { name: "name", type: "string" },
  { name: "version", type: "string" },
  { name: "verifyingContract", type: "address" },
  { name: "salt", type: "bytes32" }
];

export const metaTransactionType = [
  { name: "nonce", type: "uint256" },
  { name: "from", type: "address" },
  { name: "functionSignature", type: "bytes" }
];

interface DomainTypeData {
  name: string;
  version: string;
  verifyingContract: string;
  salt: string;
}

/**
 * Creates EIP712 Domain Data Object for Meta Transaction
 * 
 * @param name domain string name
 * @param version contract version
 * @param contractAddress Ethereum Contract Address, an EthereumAddress instance
 * @param chainId chain ID of ethereum network deployment
 * @returns constructed domain data
 */
export const createDomainData = (
    name: string, 
    version: string, 
    contractAddress: EthereumAddress,
    chainId: number):DomainTypeData => {
  return {
    name,
    version,
    verifyingContract: contractAddress.$,
    salt: "0x" + Web3.utils.toHex(chainId).replace("0x", "").padStart(64, "0")
  }
}

interface MessageTypeData {
  nonce: number;
  from: string;
  functionSignature: string;
}

interface MetaTxTypedData {
  types: {
    EIP712Domain: typeof domainType,
    MetaTransaction: typeof metaTransactionType
  },
  primaryType: 'MetaTransaction';
  domain: DomainTypeData,
  message: MessageTypeData
}

export const createTypedData = (domain:DomainTypeData, message: MessageTypeData): MetaTxTypedData => {
  return {
    types: {
      EIP712Domain: domainType,
      MetaTransaction: metaTransactionType
    },
    primaryType: 'MetaTransaction',
    domain,
    message
  }
}

export const signTypedData = async (chainConfig:ChainNetConfig, data: MetaTxTypedData) => {
  return await createTypedSignature(
    data,
    getHDWalletProvider(chainConfig) );
}