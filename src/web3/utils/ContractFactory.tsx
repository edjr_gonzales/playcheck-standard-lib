import { ContractOptions } from 'web3-eth-contract';
import { WebsocketProviderOptions, HttpProviderOptions } from 'web3-core-helpers';
import ChainNetConfig from "../types/ChainNetConfig";
import loadContractJsonAbi from "./AbiLoader";

import getWeb3, { 
  PROVIDER_FACTORIES,
} from "./Web3Factory";
import { getBunyanLogger } from "../..";
import { Web3ProviderType } from "..";

const abiCache:{
  [key:string]: any;
} = {};

const logger = getBunyanLogger('contract-factory', { env: process.env.NODE_ENV } );

export interface ChainConfigOverride {
  httpOptions?:HttpProviderOptions;
  wsOptions?:WebsocketProviderOptions;
  useProvider?:Web3ProviderType;
  options?: {
    gas?: number;
    gasPrice?:string;
    from?:string;
  }
}

export default (contractName:string, networkConfig:ChainNetConfig, overrides:ChainConfigOverride = {}) => {
  const abiCacheKey = `${contractName}/${networkConfig.networkId}`;
  if(!abiCache[abiCacheKey]){
    abiCache[abiCacheKey] = loadContractJsonAbi(contractName, networkConfig.networkId);
  }
  const providerFactory = PROVIDER_FACTORIES[(overrides.useProvider || networkConfig.useProvider)];

  const web3 = getWeb3(networkConfig, providerFactory);
  const options:ContractOptions = { ...networkConfig.options, ...overrides.options };

  if(!abiCache[abiCacheKey] || !abiCache[abiCacheKey].networks[networkConfig.networkId]){
    const error = new Error(`${contractName} is not deployed to network ${networkConfig.networkId}`);
    logger.error(error);
    throw error;
  }

  return {
    contract: new web3.eth.Contract(abiCache[abiCacheKey].abi, abiCache[abiCacheKey].networks[networkConfig.networkId].address, options),
    web3
  };
}