import { WebsocketProviderOptions, HttpProviderOptions } from 'web3-core-helpers';
import { Web3ProviderType } from '..';

export default interface ChainNetConfig {
  walletOptions: {
    mnemonic?: {
      phrase:string;
    },
    privateKeys:Array<string>;
    addressIndex:number;
    numberOfAddresses:number;
    pollingInterval?:number;
    derivationPath?: string;
  };
  httpUrl: Array<string>;
  httpUrlIdx?: number;
  wsUrl?: Array<string>;
  wsUrlIdx?: number;
  ipcUrl?: string;
  httpOptions?:HttpProviderOptions;
  wsOptions?:WebsocketProviderOptions;
  useProvider: Web3ProviderType;
  networkId:number;
  networkBlockSteps?:number;
  options: {
    gas?: number;
    gasPrice?:string;
  }
}