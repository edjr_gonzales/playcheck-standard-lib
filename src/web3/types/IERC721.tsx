import TokenId from "./TokenId";
import EthereumAddress from "../EthereumAddress";
import Wallet from "../Wallet";
import IToken from "./IToken";

export default interface IERC721 extends IToken {
  baseUri: () => Promise<string>;
  tokenURI: (tokenId:TokenId) => Promise<string>;
  balanceOf: (owner:Wallet) => Promise<number>;
  ownerOf: (tokenId:TokenId) => Promise<EthereumAddress>;
  tokenOfOwnerByIndex: (owner:EthereumAddress, index:number) => Promise<TokenId>;
  totalSupply: () => Promise<number>;
  tokenByIndex: (index:number) => Promise<TokenId>;
  approve: (to:EthereumAddress, tokenId: TokenId) => Promise<any>;
  getApproved: (tokenId:TokenId) => Promise<EthereumAddress>;
  setApprovalForAll: (operator:EthereumAddress, approved:boolean) => Promise<any>;
  isApprovedForAll: (owner:EthereumAddress, operator:EthereumAddress) => Promise<boolean>;
  transferFrom: (from:EthereumAddress, to:EthereumAddress, tokenId:TokenId) => Promise<any>;
  safeTransferFrom: (from:EthereumAddress, to:EthereumAddress, tokenId:TokenId, data?:Buffer) => Promise<any>;
}