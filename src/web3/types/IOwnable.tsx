import EthereumAddress from "../EthereumAddress";

export default interface IOwnable {
  owner: () => EthereumAddress;
}