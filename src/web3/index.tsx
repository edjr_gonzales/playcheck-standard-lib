export * from './contracts';
export * from './types';
export * from './utils';
export * from './exceptions';
export { default as Address } from './Address';
export { default as EthereumAddress } from './EthereumAddress';
export { default as Wallet } from './Wallet';

import HDWalletProvider  from '@truffle/hdwallet-provider';
import Web3 from 'web3';

import fs from "fs";
import { getBunyanLogger } from '../logging/bunyan';
import { ChainNetConfig } from './types';
import web3CallWrapper from "./utils/Web3CallWrapper";
import { hdWalletProviderFactory, PROVIDER_FACTORIES } from './utils/Web3Factory';

require('dotenv').config();

const logger = getBunyanLogger('utils-web3', { env: process.env.NODE_ENV });

export type Web3ProviderType = 'http' | 'ws' | 'ipc' | 'hdwallet' | 'wsHDwallet';
export type SignedTypeData = "eth_signTypedData_v1" | "eth_signTypedData_v2" | "eth_signTypedData_v3" | "eth_signTypedData_v4";

export const HTTP_PROVIDER = "http";
export const WS_PROVIDER = "ws";
export const IPC_PROVIDER = "ipc";
export const HD_WALLET_PROVIDER = "hdwallet";
export const HD_WALLET_PROVIDER_WS = "wsHDwallet";

interface EthereumConnectionMapping {
  [id:string]:ChainNetConfig
}

const loadEthereumConfigurations = () => {
  try {
    const loadedMapping:EthereumConnectionMapping = JSON.parse(fs.readFileSync('./ethereum-connections.json').toString());

    //clear existing mapping
    Object.keys(ethereumConfigList).forEach(key => {
      delete(ethereumConfigList[key]);
    });

    //re-map configs
    Object.keys(loadedMapping).forEach(key => {
      ethereumConfigList[key] = loadedMapping[key];
      logger.debug("loaded ethereum connection config ", key);
    });
  } catch(e) {
    logger.error("ethereum connections error", e);
  }
}

const ethereumConfigList:EthereumConnectionMapping = {};
export const getEthereumConfig = (configKey:string):ChainNetConfig => {
  logger.debug("loading ethreum config key", configKey);
  logger.debug("existing configurations", Object.keys(ethereumConfigList).length);

  if( Object.keys(ethereumConfigList).length === 0 || !ethereumConfigList[configKey] ){
    logger.debug("pre loading ethreum configurations");
    loadEthereumConfigurations();
  }

  // return a deep clone
  return JSON.parse(JSON.stringify(ethereumConfigList[configKey]));
}

const getEthereumConfigByNetworkId = (networkId:number):ChainNetConfig | false => {
  if( Object.keys(ethereumConfigList).length === 0 ){
    loadEthereumConfigurations();
  }

  const configKeys = Object.keys(ethereumConfigList);
  for(let i = 0; i < configKeys.length; i++){
    const configKey = configKeys[i];

    if(ethereumConfigList[configKey].networkId == networkId){
      // return a deep clone
      return JSON.parse(JSON.stringify(ethereumConfigList[configKey]));
    }
  }

  return false;
}

export const getHDWalletProvider = (config:ChainNetConfig | string) => {
  //logger.info("get hdwallet instance", config);

  const connectionConfig:ChainNetConfig = typeof config === 'string' ? getEthereumConfig(config) : config;

  switch(connectionConfig.useProvider){
    case 'http': 
    case 'hdwallet':
      return hdWalletProviderFactory(connectionConfig);
    case 'ws':
    case 'wsHDwallet':
      return hdWalletProviderFactory(connectionConfig, 'ws');
    default:
      if(connectionConfig.httpUrl.length > 0){
        return hdWalletProviderFactory(connectionConfig);
      } else if((connectionConfig.wsUrl?.length || 0) > 0) {
        return hdWalletProviderFactory(connectionConfig, 'ws');
      } else {
        throw new Error("Cannot create HDWalletProvider with this settings.");
      }
  }
}

export const createTypedSignature = async (msgParams:any, hdwallet:HDWalletProvider, signType:SignedTypeData = "eth_signTypedData_v4"):Promise<string|false> => {
  return new Promise<string|false>((resolve, reject) => {
    hdwallet.send({
      method: signType,
      params: [msgParams, hdwallet.getAddress()],
      from: hdwallet.getAddress(),
    }, (err:Error, result:any) => {
      if(err){
        logger.error(err);
        resolve(false);
      } else {
        logger.debug("signing result", result);
        resolve(result.result);
      }
    });
  })
}

export const isValidEthTransactionHash = async (txHash:string, networkId?:number) => {
  const patternOk = /^0x([A-Fa-f0-9]{64})$/.test(txHash);

  if(!!networkId && patternOk){
    // check if this is a valid transaction hash on network
    const networkConfig = getEthereumConfigByNetworkId(networkId);
    if(networkConfig){
      const web3 = new Web3(getHDWalletProvider(networkConfig));

      //get transaction details
      try {
        const tx = await web3.eth.getTransaction(txHash);
        if(tx){
          return true;
        }
      } catch(err) {}
      
      return false;

    } else {
      logger.error(`No network configuration found for networkId ${networkId}`);
    }
  } else {
    return patternOk;
  }
}

export const getCurrentBlock = async (config:ChainNetConfig) => {
  return await web3CallWrapper( async (web3:Web3) => {
    //eth call, use infura rotator
    return await web3.eth.getBlockNumber();
  }, config, PROVIDER_FACTORIES.http, 'getting current block on ETH');
}

