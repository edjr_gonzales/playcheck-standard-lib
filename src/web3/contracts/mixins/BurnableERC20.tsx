import ERC20 from "../ERC20";
import EthereumAddress from "../../EthereumAddress";
import IBurnableERC20 from "../../types/IBurnableERC20";

type GConstructor<T = {}> = new (...args: any[]) => T;
type ERC20Type = GConstructor<ERC20>;

export default function <TBase extends ERC20Type>(Base: TBase) {
  return class BurnableERC20 extends Base implements IBurnableERC20 {
    async burn(account:EthereumAddress, amount:number){
      await this.contract.methods.mint(account.$, amount).send();
    }
  };
}
