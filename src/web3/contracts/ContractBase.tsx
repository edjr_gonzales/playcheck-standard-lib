import Web3 from "web3";
import Bunyan from "bunyan";
import { Contract, Filter } from 'web3-eth-contract';
import HDWalletProvider from "@truffle/hdwallet-provider";

import DAppArtifacts from "../types/DAppArtifacts";
import mnemonicConverter from '../utils/MnemonicConverter';
import pKeyConverter from '../utils/PrivateKeyConverter';

import { ChainConfigOverride, getContract, loadContractJsonAbi} from "../utils";
import { ChainNetConfig } from "../types";
import { EthereumAddress } from "..";
import { InsufficientGasError, LowGasPriceError } from "../exceptions";
import web3CallWrapper from "../utils/Web3CallWrapper";

export default class ContractBase {
  _artifacts:DAppArtifacts;
  _logger:Bunyan;
  _cache:any = {};

  constructor(artifacts:DAppArtifacts){
    this._artifacts = artifacts;
    this._logger = Bunyan.createLogger({
      name: `Contract#${artifacts.contractName}`,
      level: process.env.NODE_ENV === 'development' ? Bunyan.DEBUG : Bunyan.INFO
    });

    this._initializeWallet().then(() => {
      //this._logger.debug(`${this.contractName} Wallet accounts initialized`, this._artifacts.addresses);
    })
  }

  async tokenName() {
    if(!this._cache.name){
      this._cache.name = await this._performContractCall(async (contract:Contract) => {
        return await contract.methods.symbol();
      });
    }
    
    return this._cache.name;
  }

  async tokenSymbol() {
    if(!this._cache.symbol){
      this._cache.symbol = await this._performContractCall(async (contract:Contract) => {
        return await contract.methods.symbol();
      });
    }
    
    return this._cache.symbol;
  }

  get address(): typeof EthereumAddress {
    if(!this._cache.address){
      this._cache.address = new EthereumAddress(this.contract.options.address);
    }
    
    return this._cache.address;
  }

  get networkConfig():ChainNetConfig {
    return this._artifacts.networkConfig;
  }

  get contractName():string {
    return this._artifacts.contractName;
  }

  get contract():Contract{
    return this._artifacts?.contract;
  }

  get web3():Web3 {
    return this._artifacts.web3;
  }

  get provider():any {
    return this._artifacts.web3.currentProvider;
  }

  get addresses():Array<EthereumAddress>{
    return this._artifacts.addresses;
  }

  get migrationHash():string {
    if(!this._cache.migrationHash){
      const ContractJSON = loadContractJsonAbi(this._artifacts.contractName, this._artifacts.networkConfig.networkId);
      this._cache.migrationHash = ContractJSON.networks[this._artifacts.networkConfig.networkId].transactionHash;
    }

    return this._cache.migrationHash;
  }

  get networkOverrides():ChainConfigOverride | undefined {
    return this._artifacts.overrides;
  }

  set networkOverrides(overrides:ChainConfigOverride | undefined) {
    this._artifacts.overrides = overrides;
  }

  /**
   * Retrieves events from past blocks
   * 
   * @param eventName name of events e.g. Transfer
   * @param startBlock optional, if not specified will auto detect starting from migration block of contract and succeeding calls will just increment from last block obtained
   * @param endBlock optional, will be detected based on latest block or increment of ChainNetConfig.networkBlockSteps - whichever is lower
   * @param filter optional filter as per web3 documentation
   */
  async getPastEvents(eventName:string, startBlock?:number, endBlock?:number, filter?:Filter){
    this._logger.debug(`${this.contractName}.getPastEvents`, startBlock, endBlock, filter);

    let fromBlock:number, toBlock:number = 0;

    if(!startBlock){
      this._logger.debug(`${this.contractName}.getPastEvents`, `detecting start block`);
      
      if(this._cache.getPastEventsToBlock){
        this._logger.debug(`${this.contractName}.getPastEvents`, `start block found from cache`);
        fromBlock = this._cache.getPastEventsToBlock + 1;

      } else {
        this._logger.debug(`${this.contractName}.getPastEvents`, `start block obtain from network`);
        fromBlock = Number(await web3CallWrapper<number>(async (web3:Web3) => {
          return (await web3.eth.getTransactionReceipt(this.migrationHash)).blockNumber;
        }, this.networkConfig));
      }
    } else {
      fromBlock = startBlock;
    }

    if(!endBlock){
      this._logger.debug(`${this.contractName}.getPastEvents`, `detecting end block`);

      //if none specified, auto detect it based on the value of start block
      toBlock = Number(await web3CallWrapper<number>(async (web3:Web3) => {
        return await web3.eth.getBlockNumber();
      }, this.networkConfig));

      const blockSteps = Number(this._artifacts.networkConfig.networkBlockSteps || 10000);
      if(toBlock - fromBlock > blockSteps){
        toBlock = fromBlock + blockSteps;
      }
    } else {
      toBlock = endBlock;
    }

    this._cache.getPastEventsFromBlock = fromBlock;
    this._cache.getPastEventsToBlock = toBlock;

    this._logger.debug(`${this.contractName}.getPastEvents`, startBlock, endBlock, filter);
    const events = await this.contract.getPastEvents(eventName, {
      fromBlock,
      toBlock,
      filter
    });

    return {
      events,
      fromBlock,
      toBlock
    };
  }

  async _initializeWallet(){
    const { networkConfig, addresses } = this._artifacts;

    if(Object.keys(addresses).length > 0) return;

    const generatedAddresses = !!networkConfig.walletOptions.mnemonic ? await mnemonicConverter(
      networkConfig.walletOptions.mnemonic.phrase, networkConfig.walletOptions.addressIndex, networkConfig.walletOptions.numberOfAddresses) :
      await pKeyConverter(networkConfig.walletOptions.privateKeys, networkConfig.walletOptions.addressIndex, networkConfig.walletOptions.numberOfAddresses) ;

    generatedAddresses.forEach((address, index) => {
      this._artifacts.addresses[index] = new EthereumAddress(address);
    });
  }

  get accounts():Array<EthereumAddress> {
    return this.addresses;
  }

  set accounts(accounts:Array<EthereumAddress>) {
    if(this.addresses.length === 0){
      this._artifacts.addresses = accounts;
    }
  }

  public async getPrimaryAccount():Promise<EthereumAddress> {
    if(Object.keys(this.addresses).length === 0){
      await this._initializeWallet();
    }

    return this.addresses[0];
  }

  async _currentGasPrice():Promise<string> {
    const gasPrice = String(await web3CallWrapper(async (web3:Web3) => {
      return await web3.eth.getGasPrice();
    }, this.networkConfig));

    return gasPrice;
  }

  /**
   * Send transaction wrapper with pre-emptive checks for gas expenditure and gas price to avoid wasted calls and losing ethereum.
   * Adding the functionality of auto retry via endpoint rotations.
   * 
   * @param contractMethodGetter a function that accepts contract object to return the method of the contract to send
   * @param functionDesc optional message to describe this call
   */
  async _performContractSend(contractMethodGetter:(contract:Contract) => any, callbacks?:{ [events:string]:Function }, functionDesc?:string):Promise<(any | null)>{
    if(functionDesc){
      this._logger.info(`[performContractSend] ${functionDesc}`);
    }

    const retryAttempts = Number(process.env.WEB3_MAX_RETRY || 10);
    let web3CallResult:any | null = null;

    await this._initializeWallet();

    let throwError = null;

    for(let i:number  = 0; i < retryAttempts; i++){
      try {
        //close current provider connections as necessary
        this.cleanUp();

        //open new set of artifacts
        const contractAtifacts = await getContract(this.contractName, this.networkConfig, this._artifacts.overrides);
        if(!contractAtifacts){
          throwError = new Error(`${this.contractName} is not deployed on blockain network ${this.networkConfig.networkId}`);
          break;
        }

        this._artifacts = { ...this._artifacts, ...contractAtifacts };

        const contractMethod = contractMethodGetter(contractAtifacts.contract);
        const contractOptions = { ...this.networkConfig.options, ...this.networkOverrides?.options };

        if(!contractOptions.from){
          contractOptions.from = (await this.getPrimaryAccount()).$;
        }
        
        this._logger.debug(`Contract Send Options`, this.networkConfig.options);
        this._logger.debug(`Contract Send Override Options`, this.networkOverrides?.options);
        this._logger.debug(`Contract Sender Option From`, contractOptions.from);

        this._logger.debug(`Contract Sender Option Gas`, contractOptions.gas);

        //gas allowance check
        if(!!contractOptions.gas && this.networkConfig.options.gas){
          let gasAllowanceRequired = this.networkConfig.options.gas;

          try {
            //apparently, this call throws error if gas supplied is not enough
            //so we will assume that gas supplied is not enough if this throws
            gasAllowanceRequired = Number(await contractMethod.estimateGas(contractOptions));

          } catch(estimateError:any) {
            //the error can also happen by other means like contract call exceptions 
            //we will isoltae that case and let outer call decide on what to do
            if(estimateError.toString().toLowerCase().indexOf("gas required exceeds allowance") < 0){
              throw estimateError;
            }
          } 
          
          this._logger.debug("Gas allowance estimate", gasAllowanceRequired);
    
          if(gasAllowanceRequired >= contractOptions.gas){
            throw new InsufficientGasError(contractOptions.gas, gasAllowanceRequired);
          }
        }

        //somehow contractOptions.gasPrice turns into hex encoded string 
        //after calling estimateGas using this option, revert the value to string
        if(contractOptions.gasPrice && contractOptions.gasPrice.startsWith("0x")){
          contractOptions.gasPrice = parseInt(contractOptions.gasPrice, 16).toString();
        }

        this._logger.debug(`Contract Sender Option GasPrice`, contractOptions.gasPrice);
    
        //gas price check
        if(contractOptions.gasPrice !== undefined){
          const currentGasPrice = await (await this._currentGasPrice()).toString();
    
          //this._logger.debug(`Gas!!`, typeof currentGasPrice, typeof contractOptions.gasPrice);
          //this._logger.debug(`Gas!!`, currentGasPrice, contractOptions.gasPrice.toString());
          if(currentGasPrice > contractOptions.gasPrice){
            throw new LowGasPriceError(contractOptions.gasPrice, currentGasPrice);
          } else {
            //set gas price as network's
            contractOptions.gasPrice = currentGasPrice;
          }
        }

        let sendCmd = contractMethod.send(contractOptions);

        if(callbacks){
          Object.keys(callbacks).forEach(event => {
            if(event !== 'then'){
              sendCmd = sendCmd.on(event, callbacks[event]);
            }
          });
        }

        if(callbacks && callbacks['then']){
          this._logger.debug(`_performContractSend callback for then specified -- `);
          // attach then handler
          sendCmd = sendCmd.then(callbacks['then']);
          throwError = null; //clear error

        } else if(typeof callbacks?.['then'] === 'undefined') {
          this._logger.debug(`_performContractSend callback for then undefined. Defining default. `);

          // attach default then handler
          sendCmd = sendCmd.then((txReceipt: any) => {
            return txReceipt;
          });

        } else {
          this._logger.debug(`_performContractSend callback for then false. Omitting. `);

          /**
           * then is set to false, dont add one
           * {
           *  'then': false
           * }
           */
        }

        sendCmd = sendCmd.catch((err:any) => {
          this._logger.error(`_performContractSend caught -- `, err);
          throwError = err;
        });
        
        web3CallResult = await sendCmd;

        break;  //break if finished without errors

      } catch (err){
        this._logger.error(`${(!!functionDesc ? functionDesc : "_performContractCall")} [error] `, err);

        throwError = err;

        if(err instanceof InsufficientGasError || err instanceof LowGasPriceError){
          break;
        }

      } finally {
        //
      }
    }

    if(throwError){
      this._logger.debug('Re-Throw', throwError);
      return Promise.reject(throwError);
      //throw throwError;
    };

    /* Promise.resolve(web3CallResult); */

    return web3CallResult;
  }

  /**
   * Call wrapper that provides auto retry with endpoint rotations.
   * 
   * @param contractMethodGetter a function that accepts contract object to return the method of the contract to call
   * @param functionDesc optional message to describe this call
   */
  async _performContractCall<T>(contractMethodGetter:(contract:Contract) => any, functionDesc?:string):Promise<(T | null)>{
    if(functionDesc){
      this._logger.info(`[performContractCall] ${functionDesc}`);
    }

    const retryAttempts = Number(process.env.WEB3_MAX_RETRY || 10);
    let web3CallResult:T | null = null;

    await this._initializeWallet();

    let throwError = null;

    for(let i:number  = 0; i < retryAttempts; i++){
      try {
        //close current provider connections as necessary
        this.cleanUp();

        //open new set of artifacts
        const contractAtifacts = await getContract(this.contractName, this.networkConfig, { options: { from: (await this.getPrimaryAccount()).$ } });
        if(!contractAtifacts){
          throwError = new Error(`${this.contractName} is not deployed on blockain network ${this.networkConfig.networkId}`);
          break;
        }

        this._artifacts = { ...this._artifacts, ...contractAtifacts };

        const methodCall = await contractMethodGetter(contractAtifacts.contract);

        //this._logger.debug(`method getter`, contractMethodGetter);
        //this._logger.debug(`method getter call `, methodCall.call);

        web3CallResult = await methodCall.call()
          .then(async (result:any) => {
            return result;
          })
          .catch(async (err:Error) => {
            return null;
          });

        break;  //break if finished without errors

      } catch (err){
        this._logger.error(`${(!!functionDesc ? functionDesc : "_performContractCall")} [error] -- `, err);
      }
    }

    if(throwError){
      this._logger.debug('Re-Throw', throwError);
      return Promise.reject(throwError);
      //throw throwError;
    };
    
    return web3CallResult;
  }

  public cleanUp(){
    /* this._logger.debug(`Clean up provider`, this.provider);
    this._logger.debug(`Typeof`, typeof this.provider); */

    if(this.provider instanceof HDWalletProvider){
      this.provider.engine.stop();
    }

    if(this.provider instanceof Web3.providers.WebsocketProvider){
      this.provider.disconnect(0, 'clean up');
    }

    if(this.provider instanceof Web3.providers.IpcProvider){
      this.provider.reset();
    }
  }
}