import { Contract } from 'web3-eth-contract';

import EthereumAddress from '../EthereumAddress';
import IERC20 from '../types/IERC20';
import DAppArtifacts from '../types/DAppArtifacts';
import Wallet from '../Wallet';
import ContractBase from "./ContractBase";

export default class ERC20 extends ContractBase implements IERC20 {
  
  constructor(artifacts:DAppArtifacts){
    super(artifacts);
  }

  async decimals(){
    if(!this._cache.decimals){
      this._cache.decimals = this._performContractCall(
        (contract:Contract) => {
          return contract.methods.decimals();
        },
        `getting ${this.contractName} decimals`
      );
    }
    
    return this._cache.decimals;
  }

  async balanceOf(wallet:Wallet){
    return Number(await this._performContractCall(
        (contract:Contract) => {
          return contract.methods.balanceOf(wallet.$);
        },
        `getting ${this.contractName} balance of ${wallet.$}`
        ) || 0);
  }

  async transfer(recipient:EthereumAddress, amount:number, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.transfer(recipient.$, amount);
      },
      callbacks,
      `${await this.getPrimaryAccount()} transferring ${amount} ${this.contractName} to ${recipient.$}`
    );
  }

  async allowance(owner:EthereumAddress, spender:EthereumAddress){
    return Number(await this._performContractCall(
      (contract:Contract) => {
        return contract.methods.allowance(owner.$, spender.$);
      },
      `getting ${owner.$} ${this.contractName} allowance for ${spender.$}`) || 0);
  }

  async approve(spender:EthereumAddress, amount:number, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.approve(spender.$, amount);
      },
      callbacks,
      `${await this.getPrimaryAccount()} approving ${spender.$} for ${amount} ${this.contractName}`
    )
  }

  async mint(recipient:EthereumAddress, amount:number, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.mint(recipient.$, amount);
      },
      callbacks,
      `minting ${amount} ${this.contractName} tokens to ${recipient.$}`
    )
  }

  async transferFrom(sender:EthereumAddress, recipient:EthereumAddress, amount:number, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.transferFrom(sender.$, recipient.$, amount);
      },
      callbacks,
      `transfer ${this.contractName} ${this.tokenSymbol} from ${sender.$} to ${recipient.$} with the amount of ${amount}`);
  }

  async increaseAllowance(spender:EthereumAddress, addedValue:number, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.increaseAllowance(spender.$, addedValue);
      },
      callbacks,
      `${await this.getPrimaryAccount()} increases ${this.contractName} allowance of ${spender.$} with another ${addedValue}`
    );
  }
  
  async decreaseAllowance(spender:EthereumAddress, removedValue:number, callbacks?:{ [event:string]:Function }){
    return this._performContractSend(
      (contract:Contract) => {
        return contract.methods.decreaseAllowance(spender.$, removedValue);
      },
      callbacks,
      `${await this.getPrimaryAccount()} descreases ${this.contractName} allowance of ${spender.$} with ${removedValue}`
    )
  }
}

