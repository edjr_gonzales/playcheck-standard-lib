import { Contract } from 'web3-eth-contract';

import EthereumAddress from '../EthereumAddress';
import DAppArtifacts from '../types/DAppArtifacts';
import IERC721 from '../types/IERC721';
import TokenId from '../types/TokenId';
import ContractBase from './ContractBase';

export default class ERC721 extends ContractBase implements IERC721 {
  
  constructor(artifacts:DAppArtifacts){
    super(artifacts);
  }

  async baseUri(){
    if(!this._cache.baseUri){
      this._cache.baseUri = await this._performContractCall(
        (contract:Contract) => {
          return contract.methods.baseUri();
        },
        `getting ${this.contractName} base URI`
      );
    }
    
    return this._cache.baseUri;
  }

  async tokenURI(tokenId:TokenId){
    return String(await this._performContractCall<string>(
      (contract:Contract) => {
        return contract.methods.tokenURI(tokenId);
      },
      `getting ${this.contractName} token URI for ${tokenId}`
      ));
  }

  async balanceOf(wallet:EthereumAddress){
    return Number(await this._performContractCall(
      (contract:Contract) => {
        return contract.methods.balanceOf(wallet.$);
      },
      `getting ${this.contractName} balance of ${wallet.$}`
      ));
  }

  async ownerOf(tokenId:TokenId){
    return new EthereumAddress(String(await this._performContractCall(
      async (contract:Contract) => {
        return await contract.methods.ownerOf(tokenId);
      },
      `getting owner of ${this.contractName} ${tokenId}`
      )));
  }

  async tokenOfOwnerByIndex(owner:EthereumAddress, index:number){
    return String(await this._performContractCall(
      (contract:Contract) => {
        return contract.methods.tokenOfOwnerByIndex(owner.$, index);
      },
      `getting token of ${owner.$} at index ${index}`
      ));
  }

  async totalSupply(){
    return Number(await this._performContractCall(
      (contract:Contract) => {
        return contract.methods.totalSupply();
      },
      `getting ${this.contractName} total supply`
      ));
  }

  async tokenByIndex(index:number){
    return Number(await this._performContractCall<number>(
      (contract:Contract) => {
        return contract.methods.tokenByIndex(index);
      },
      `getting ${this.contractName} token by index ${index}`
      ));
  }

  async approve(spender:EthereumAddress, tokenId:TokenId, callbacks?:{ [event:string]:Function }){
    return this._performContractSend((contract:Contract) => {
        return contract.methods.approve(spender.$, tokenId);
      }, callbacks, `${await this.getPrimaryAccount()} approves ${spender.$} for ${this.contractName} ${tokenId}`);
  }

  async getApproved(tokenId:TokenId){
    return new EthereumAddress(String(await this._performContractCall(
      (contract:Contract) => {
        return contract.methods.getApproved(tokenId);
      },
      `getting whose approved for token ${this.contractName} ${tokenId}`
      ))
    );
  }

  async setApprovalForAll(operator:EthereumAddress, approved:boolean, callbacks?:{ [event:string]:Function }){
    return this._performContractSend((contract:Contract) => {
      return contract.methods.setApprovalForAll(operator.$, approved);
    }, callbacks, `${await this.getPrimaryAccount()} set approval to all of ${this.contractName} to ${operator.$} into ${approved}`);
  }

  async isApprovedForAll(owner:EthereumAddress, operator:EthereumAddress){
    return Boolean(await this._performContractCall(
      (contract:Contract) => {
        return contract.methods.isApprovedForAll(owner.$, operator.$);
      },
      `checking if ${operator.$} is approved for ${owner.$}'s assets`
      ));
  }

  async transferFrom(from:EthereumAddress, to:EthereumAddress, tokenId:TokenId, callbacks?:{ [event:string]:Function }){
    return this._performContractSend((contract:Contract) => {
      return contract.methods.transferFrom(from.$, to.$, tokenId);
    }, callbacks, `${await this.getPrimaryAccount()} transfers ${this.contractName} ${tokenId} from ${from.$} to ${to.$}`);
  }

  async safeTransferFrom(from:EthereumAddress, to:EthereumAddress, tokenId:TokenId, data?:Buffer, callbacks?:{ [event:string]:Function }){
    const msg = `${await this.getPrimaryAccount()} safe transfers ${this.contractName} ${tokenId} from ${from.$} to ${to.$}`;

    if(data){
      return this._performContractSend(
        (contract:Contract) => {
          return contract.methods.safeTransferFrom(from.$, to.$, tokenId, data);
        }, callbacks, msg);
    } else {
      return this._performContractSend(
        (contract:Contract) => {
          return contract.methods.safeTransferFrom(from.$, to.$, tokenId);
        }, callbacks, msg);
    }
  }
}
