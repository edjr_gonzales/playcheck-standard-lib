import { Contract } from 'web3-eth-contract';
import { ContractBase } from '.';

import { EthereumAddress } from '..';

export interface IMetaTransactable {
  version: () => Promise<number>;
  getChainId: () => Promise<number>;
  getNonce: (u: EthereumAddress) => Promise<number>;
  getDomainSeperator: () => Promise<any>;
  executeMetaTransaction: (
    u: EthereumAddress,
    f: string,
    r: string,
    s: string,
    v: string
  ) => Promise<any>;
}

type GConstructor<T = {}> = new (...args: any[]) => T;
type MetaTransactable = GConstructor<ContractBase>;

export const MetaTransactable = <TBase extends MetaTransactable>( Base: TBase ) => {
  return class extends Base implements IMetaTransactable {
    async version(){
      return (await this._performContractCall<number>(
          (contract:Contract) => {
            return contract.methods.version();
          },
          `getting ${this.contractName} contract's version`
          )) || NaN;
    }

    async getChainId(){
      return (await this._performContractCall<number>(
          (contract:Contract) => {
            return contract.methods.getChainId();
          },
          `getting ${this.contractName} contract's chain ID`
          )) || NaN;
    }
  
    async getNonce(userAddress: EthereumAddress){
      return (await this._performContractCall<number>(
          (contract:Contract) => {
            return contract.methods.getNonce(userAddress.$);
          },
          `getting meta transaction nonce for user ${userAddress.$} on contract ${this.contractName}`
          ) || NaN);
    }
  
    async getDomainSeperator(){
      return await this._performContractCall<string>(
          (contract:Contract) => {
            return contract.methods.getDomainSeperator();
          },
          `getting ${this.contractName} contract's domain separator`
          );
    }
  
    async executeMetaTransaction(
      userAddress: EthereumAddress,
      functionSignature: string,
      r: string,
      s: string,
      v: string
    ){
      return await this._performContractSend(
          (contract:Contract) => {
            return contract.methods.executeMetaTransaction(
              userAddress.$, functionSignature, r, s, v
            );
          },
          {
            'transactionHash': ((txHash:string) => txHash)
          },
          `executing meta transaction on contract${this.contractName} on behalf of ${userAddress}`
          );
    }
  };
};

