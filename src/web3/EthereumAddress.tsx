import { getBunyanLogger } from "..";
import Address from "./Address";

const logger = getBunyanLogger('eth-address-class');

export default class EthereumAddress {
  protected _wallet:string;

  constructor(walletAddr:string){
    const wallet = Address(walletAddr);
    if(!wallet) {
      logger.error(`Failed initiating wallet address -- ${walletAddr}`);
      throw new Error(`Failed initiating wallet address -- ${walletAddr}`);
    }

    this._wallet = wallet;
  }

  toString(){ return this._wallet }

  valueOf(){ return this._wallet }

  get value(){ 
    return this.valueOf();
  }
  
  get $(){ 
    return this.value;
  }

  is(compareTo: string | EthereumAddress){
    if(typeof compareTo === 'string'){
      return this._wallet?.toLowerCase() === compareTo.toLowerCase();
    } else {
      return this._wallet === compareTo.valueOf();
    }
  }

  equals(compareTo: string | EthereumAddress){
    return this.is(compareTo);
  };
}