import Web3 from "web3";

export default (chainAddr:string):string | false => {
  const ethAddrPattern = /^0x[a-fA-F0-9]{40}$/g;
  if(ethAddrPattern.test(chainAddr)){
    return Web3.utils.toChecksumAddress(chainAddr);
  } else {
    //throw new Error(`Invalid Ethereum Address -- ${chainAddr}`);
    return false;
  }
}