export default class InsufficientGasError extends Error {
  constructor(gasAlloted:number, gasRequired:number){
    super(`Insufficient Gas - Limit: ${gasAlloted} Required: ${gasRequired}`);
  }
}