import { ClassConstructor } from "./types";

export const pause = (ms:number = 1000) => {
  return new Promise(resolve => setTimeout(resolve, ms));
}

/**
 * Number to Hex
 * @param d number value
 * @param len length of hex string minimum
 * @returns hex string
 */
export const n2h = (d:number, len:number) => {
  var s = (+d).toString(16);

  while(s.length < len) {
      s = '0' + s;
  }

  return s;
}

const primitives = ['string', 'number', 'bigint', 'boolean', 'undefined', 'null'];
export const isPrimitive = (val:any) => {
  return primitives.includes(typeof val);
}

/**
 * 
 * @param val checks if value is null, undefined or empty string
 * @returns boolean
 */
export const isNothing = (val:any) => {
  return val === undefined || val === null || (typeof val === 'string' && val.trim() === '') || isNaN(val);
}

export const obj2Type = <T extends ClassConstructor<T>>(obj:any, TypeConstructor: T) => {
  switch(TypeConstructor.name){
    case 'String':
      return String(obj);
    case 'Number':
      return Number(obj);
    case 'Boolean':
      return Boolean(obj);
    case 'BigInt':
      return BigInt(obj);
    default:
      return Object.assign(new TypeConstructor, (typeof obj === 'string') ? JSON.parse(obj) : obj);
  }
}

export const ancestors = (anyclass:ClassConstructor<any>): ClassConstructor<any>[] => {
  switch (true) {
    case (anyclass === undefined || anyclass === null): return [];
    default:
      return [anyclass, ...(ancestors (Object.getPrototypeOf (anyclass)))];
  }
}

export const isNumeric = (str:any) => {
  return typeof str === 'number' || (typeof str === 'string' && !isNaN(parseInt(str)) && !isNaN(parseFloat(str)));
}

export * from "./types";
export * from "./logging/bunyan";
export * from "./db";
export * from "./envtools";
export * from "./web3";