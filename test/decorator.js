const { WalletAccount, AccountInfo, DynamoDbTable, DynamoDbTableHash, DynamoDbTableRange, DynamoDbTableAttr, TestClass, scanMappedClass } = require("../lib/main.js");

const chai = require('chai');
const should = chai.should();
const assert = chai.assert;
const expect = chai.expect;

describe('DynamoDbTable', () => {

  it('produces correct output for saving', (done) => {
    const test = Object.assign(new TestClass, {
      testKey: '0x41e5f0Fcb34482859501c0769e1949b7798c43BF',
      testSort: '0x41e5f0Fcb34482859501c0769e1949b7798c43BF',
      stringMapAttr: {
        key1: 'testkey1',
        key2: 'testkey2',
        key3: 'testkey3',
      },
      stringListAttr: ['item1', 'item2', 'item3','item4', 'item4', 'item6']
    });

    // set this values as loaded
    test._isNew = false;
    test._memoize();

    // console.log('is key 1 changed', test._isChanged('stringMapAttr'));

    test.stringMapAttr['key1'] = 'changed1';
    
    expect(test._isChanged('stringMapAttr')).to.be.true;

    const saveData = test._buildData();
    console.log('saveData', saveData);

    expect(saveData[`stringMapAttr`].key1).to.equal('changed1'); // cause it changed
    expect(saveData[`stringMapAttr`].key2).to.undefined; // cuase it did not changed
    expect(Object.keys(saveData).length).to.equal(2);

    // perform array updates

    // list change test
    test.stringListAttr[0] = 'item1#';
    test.stringListAttr.pop();
    test.stringListAttr.pop();
    test.stringListAttr.pop();
    test.stringListAttr.push('item-inserted1');
    test.stringListAttr.push('item-inserted2');
    
    console.log('Array changes', test.stringListAttr);

    test.save(true)
      .then(saveRes => {
        saveRes.forEach((params) => {
          console.log('Saving params 1', params);
        });

        // expecting a single remove (5th item)
        assert.isNotFalse(saveRes[0]);
        assert.isArray(saveRes[0]);
        assert.equal(saveRes[0].length, 1);
        expect(saveRes[0][0].UpdateExpression).to.equal('REMOVE #00000[5]');

        // because there is no additions expected
        expect(saveRes[1]).to.be.false;

        // contains proper updates
        assert.isNotFalse(saveRes[2]);
        
        // check appendage
        test.stringListAttr.push('item-inserted3');
        test.stringListAttr.push('item-inserted4');
        test.stringListAttr.push('item-inserted5');

        test.save(true).then(saveRes2 => {
          saveRes2.forEach((params) => {
            console.log('Saving params 2', params);
          });

          // no deletions happened
          expect(saveRes2[0]).to.be.false;

          expect(saveRes2[1].UpdateExpression).to.equal('#00000 = list_append(#00000, :00000)');

          // contains proper updates
          assert.isNotFalse(saveRes2[2]);

          done();
        }).catch((err) => {
          done(err);
        });

      }).catch((err) => {
        done(err);
      });
  });

  it('decorates WalletAccount class properly', (done) => {
    try {
      const account = new WalletAccount();
      account.ethWallet = "0x41e5f0Fcb34482859501c0769e1949b7798c43BF";
      //console.log("Account", account);
      expect(account.load).to.be.an.instanceof(Function);
      expect(account.save).to.be.an.instanceof(Function);
      expect(account.destroy).to.be.an.instanceof(Function);
      expect(account.table).to.be.an.instanceof(Function);

      //account.sessionTime = (new Date()).toString();
      //console.log("Account", account);

      //load record and see if its gets populated
      //account.inspectDynamo();

      account.load().then(() => {
        //console.log("Loaded Account", account);
        expect(account._isNew).to.be.false;
        done();
      }).catch((err) => {
        //console.log("Failed Account", account, err);
        done(err);
      });

    } catch (err){
      //console.log("Error", err);
      done(err);
    }
    
  });

  it('can load an actual record', (done) => {
    try {
      const account = new WalletAccount();
      account.ethWallet = "0x3233f67E541444DDbbf9391630D92D7F7Aaf508D";
      
      account.load().then(() => {
        expect(account._isNew).to.be.false;
        done();
      }).catch((err) => {
        done(err);
      });

    } catch (err){
      done(false);
    }
    
  });

  it('can properly marked values that only changed', (done) => {

    try {
      const account = new WalletAccount();
      account.ethWallet = "0x3233f67E541444DDbbf9391630D92D7F7Aaf508D";
      
      account.load().then(() => {
        expect(account._isNew).to.be.false;

        account.sessionTime = (new Date()).toString();

        const changedData = account._buildData();

        expect('sessionTime' in changedData).to.be.true;
        expect(Object.keys(changedData).length).to.equal(1);

        done();
      }).catch((err) => {
        done(err);
      });

    } catch (err){
      done(false);
    }
    
  });

  it('can update record without problems', (done) => {

    try {
      const account = new WalletAccount();
      const sameAccount = new WalletAccount();

      const newValue = (new Date()).toString();

      account.ethWallet = "0x3233f67E541444DDbbf9391630D92D7F7Aaf508D";
      
      account.load()
      .then(() => {
        expect(account._isNew).to.be.false;

        expect(account.sessionTime).to.not.equal(newValue);
        account.sessionTime = newValue;
        return account.save();

      }).then(() => {
        Object.assign(sameAccount, { ethWallet: account.ethWallet });
        return sameAccount.load();

      }).then(() => {
        expect(sameAccount.sessionTime).to.equal(newValue);
        done();

      }).catch((err) => {
        done(err);
      });

    } catch (err){
      done(err);
    }
    
  });

  it('shows inheritance works with all inherited attributes present', (done) => {

    try {
      const account = new AccountInfo();
      
      account.ethWallet = "0x3233f67E541444DDbbf9391630D92D7F7Aaf508D";
      account.displayName = "Jeds";
      
      const columns = account._inspect();
      
      expect(columns.sessionTime).to.not.undefined;
      expect(columns.displayName).to.not.undefined;
      
      done();
    } catch (err){
      done(err);
    }
  });

  it('can scan items without problems', async() => {

    let count = 0;
    const items = await scanMappedClass({}, WalletAccount);
    for await (let item of items){
      count++;
    }

    console.log('total', count);

    expect(count).to.be.greaterThan(0);
  });
});